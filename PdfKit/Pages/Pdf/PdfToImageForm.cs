﻿using Base.Platform;
using FileKit.Common;
using PdfiumViewer;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Pdf
{
    public partial class PdfToImageForm : UIForm
    {
        public PdfToImageForm(Module module)
        {
            InitializeComponent();
        }

        private void btnSelectPdf_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Pdf文件|*.pdf" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tPdfPath.Text = openFileDialog.FileName;
                }
            }
        }

        private void btnViewPdf_Click(object sender, EventArgs e)
        {
            this.showPdfView(tPdfPath.Text);
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            string path = tPdfPath.Text;
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.pdf2Pic(path);
        }

        private string dirPath;
        public void pdf2Pic(string pdfPath)
        {
            uiGroupBoxMain.Enabled = false;
            lbMsg.Visible = true;
            lbMain.Items.Clear();
            dirPath = null;
            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                string fileName = System.IO.Path.GetFileNameWithoutExtension(pdfPath);
                dirPath = Path.Combine(System.IO.Path.GetDirectoryName(pdfPath), fileName);
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                using (PdfiumViewer.PdfDocument pdfDoc = PdfiumViewer.PdfDocument.Load(pdfPath))
                {
                    int pdfPage = pdfDoc.PageCount;
                    var pageSizes = pdfDoc.PageSizes;
                    for (int index = 0; index < pdfPage; index++)
                    {
                        string imgPath = System.IO.Path.Combine(dirPath, (index + 1) + ".png");
                        Base.Common.Utils.Invoke(lbMsg, () =>
                        {
                            lbMain.Items.Insert(0, imgPath);
                            lbMsg.Text = string.Format("正在处理第 {0} 页", index + 1);
                        });
                        if (File.Exists(imgPath))
                            File.Delete(imgPath);
                        Size size = pageSizes[index].ToSize();
                        System.Drawing.Image image = pdfDoc.Render(index, size.Width, size.Height, PdfRenderFlags.CorrectFromDpi);
                        image.Save(imgPath, ImageFormat.Png);
                    }
                }
            }, (s2, e2) =>
            {
                lbMsg.Text = "完成";
                ShowSuccessDialog("提示", "转化完成！");
                uiGroupBoxMain.Enabled = true;
                lbMsg.Visible = false;
            });
        }

        private void showPdfView(string path, string title = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Utils.ShowPdf(path);
            //PdfView pdfView = new PdfView(path, title);
            //pdfView.Show();
        }

        private void btnOpenDir_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(dirPath))
                System.Diagnostics.Process.Start("explorer.exe", dirPath);
            else
                ShowWarningDialog("提示", "请先选择文件进行转化！");
        }

        private void lbMain_MouseMove(object sender, MouseEventArgs e)
        {
            int index = lbMain.IndexFromPoint(e.Location);
            if (index != -1 && index < lbMain.Items.Count)
            {
                if (uiToolTipMain.GetToolTip(lbMain) != lbMain.Items[index].ToString())
                {
                    uiToolTipMain.SetToolTip(lbMain, lbMain.Items[index].ToString());
                }
            }
        }

        private void showFile(string filePath)
        {
            string extension = System.IO.Path.GetExtension(filePath) + "";
            if (extension.ToUpper() == "pdf")
                this.showPdfView(filePath);
            else
                Utils.ShowImage(filePath);
        }

        private void lbMain_ItemDoubleClick(object sender, EventArgs e)
        {
            this.showFile(lbMain.SelectedItem.ToString());
        }
    }
}
