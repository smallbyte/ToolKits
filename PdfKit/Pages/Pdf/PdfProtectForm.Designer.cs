﻿namespace FileKit.Pages.Pdf
{
    partial class PdfProtectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PdfProtectForm));
            this.tDecryptPdf = new Sunny.UI.UITextBox();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiCheckBoxGroupMain = new Sunny.UI.UICheckBoxGroup();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.tEncryptReadPass = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.btnEncryptProtect = new Sunny.UI.UIButton();
            this.btnViewEncryptPdf = new Sunny.UI.UISymbolButton();
            this.btnEncryptPdf = new Sunny.UI.UISymbolButton();
            this.btnRemovePassProtect = new Sunny.UI.UIButton();
            this.btnViewDecryptPdf = new Sunny.UI.UISymbolButton();
            this.btnDecryptPdf = new Sunny.UI.UISymbolButton();
            this.tDecryptEditPass = new Sunny.UI.UITextBox();
            this.tEncryptEditPass = new Sunny.UI.UITextBox();
            this.tEncryptPdfPath = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.groupBoxDecrypt = new Sunny.UI.UIGroupBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.groupBoxEncrypt = new Sunny.UI.UIGroupBox();
            this.groupBoxDecrypt.SuspendLayout();
            this.groupBoxEncrypt.SuspendLayout();
            this.SuspendLayout();
            // 
            // tDecryptPdf
            // 
            this.tDecryptPdf.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tDecryptPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tDecryptPdf.Location = new System.Drawing.Point(120, 35);
            this.tDecryptPdf.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tDecryptPdf.MinimumSize = new System.Drawing.Size(1, 16);
            this.tDecryptPdf.Name = "tDecryptPdf";
            this.tDecryptPdf.ReadOnly = true;
            this.tDecryptPdf.ShowText = false;
            this.tDecryptPdf.Size = new System.Drawing.Size(397, 32);
            this.tDecryptPdf.TabIndex = 21;
            this.tDecryptPdf.TagString = "";
            this.tDecryptPdf.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tDecryptPdf.Watermark = "";
            this.tDecryptPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel6.Location = new System.Drawing.Point(19, 36);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(107, 27);
            this.uiLabel6.TabIndex = 19;
            this.uiLabel6.Text = "文件路径:";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel6.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiCheckBoxGroupMain
            // 
            this.uiCheckBoxGroupMain.ColumnCount = 2;
            this.uiCheckBoxGroupMain.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiCheckBoxGroupMain.Location = new System.Drawing.Point(24, 154);
            this.uiCheckBoxGroupMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiCheckBoxGroupMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiCheckBoxGroupMain.Name = "uiCheckBoxGroupMain";
            this.uiCheckBoxGroupMain.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiCheckBoxGroupMain.SelectedIndexes = ((System.Collections.Generic.List<int>)(resources.GetObject("uiCheckBoxGroupMain.SelectedIndexes")));
            this.uiCheckBoxGroupMain.Size = new System.Drawing.Size(748, 105);
            this.uiCheckBoxGroupMain.TabIndex = 0;
            this.uiCheckBoxGroupMain.Text = "权限（默认不允许）";
            this.uiCheckBoxGroupMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiCheckBoxGroupMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel4.ForeColor = System.Drawing.Color.Red;
            this.uiLabel4.Location = new System.Drawing.Point(331, 111);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(120, 27);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel4.StyleCustomMode = true;
            this.uiLabel4.TabIndex = 22;
            this.uiLabel4.Text = "（可为空）";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel4.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tEncryptReadPass
            // 
            this.tEncryptReadPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tEncryptReadPass.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tEncryptReadPass.Location = new System.Drawing.Point(120, 112);
            this.tEncryptReadPass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tEncryptReadPass.MinimumSize = new System.Drawing.Size(1, 16);
            this.tEncryptReadPass.Name = "tEncryptReadPass";
            this.tEncryptReadPass.PasswordChar = '*';
            this.tEncryptReadPass.ShowText = false;
            this.tEncryptReadPass.Size = new System.Drawing.Size(202, 32);
            this.tEncryptReadPass.TabIndex = 20;
            this.tEncryptReadPass.TagString = "";
            this.tEncryptReadPass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tEncryptReadPass.Watermark = "查看Pdf的密码";
            this.tEncryptReadPass.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel3.Location = new System.Drawing.Point(19, 110);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(120, 27);
            this.uiLabel3.TabIndex = 19;
            this.uiLabel3.Text = "打开密码：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel3.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnEncryptProtect
            // 
            this.btnEncryptProtect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEncryptProtect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnEncryptProtect.Location = new System.Drawing.Point(535, 77);
            this.btnEncryptProtect.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnEncryptProtect.Name = "btnEncryptProtect";
            this.btnEncryptProtect.Size = new System.Drawing.Size(237, 60);
            this.btnEncryptProtect.TabIndex = 18;
            this.btnEncryptProtect.Text = "添加密码";
            this.btnEncryptProtect.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnEncryptProtect.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnEncryptProtect.Click += new System.EventHandler(this.btnEncryptProtect_Click);
            // 
            // btnViewEncryptPdf
            // 
            this.btnViewEncryptPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewEncryptPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewEncryptPdf.Location = new System.Drawing.Point(664, 40);
            this.btnViewEncryptPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnViewEncryptPdf.Name = "btnViewEncryptPdf";
            this.btnViewEncryptPdf.Size = new System.Drawing.Size(109, 32);
            this.btnViewEncryptPdf.Symbol = 61550;
            this.btnViewEncryptPdf.TabIndex = 17;
            this.btnViewEncryptPdf.Text = "预览";
            this.btnViewEncryptPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewEncryptPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnViewEncryptPdf.Click += new System.EventHandler(this.btnViewEncryptPdf_Click);
            // 
            // btnEncryptPdf
            // 
            this.btnEncryptPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEncryptPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnEncryptPdf.Location = new System.Drawing.Point(534, 40);
            this.btnEncryptPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnEncryptPdf.Name = "btnEncryptPdf";
            this.btnEncryptPdf.Size = new System.Drawing.Size(122, 32);
            this.btnEncryptPdf.Symbol = 61564;
            this.btnEncryptPdf.TabIndex = 16;
            this.btnEncryptPdf.Text = "选择文件";
            this.btnEncryptPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnEncryptPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnEncryptPdf.Click += new System.EventHandler(this.btnEncryptPdf_Click);
            // 
            // btnRemovePassProtect
            // 
            this.btnRemovePassProtect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemovePassProtect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRemovePassProtect.Location = new System.Drawing.Point(538, 73);
            this.btnRemovePassProtect.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnRemovePassProtect.Name = "btnRemovePassProtect";
            this.btnRemovePassProtect.Size = new System.Drawing.Size(235, 59);
            this.btnRemovePassProtect.TabIndex = 25;
            this.btnRemovePassProtect.Text = "移除密码";
            this.btnRemovePassProtect.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRemovePassProtect.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnRemovePassProtect.Click += new System.EventHandler(this.btnRemovePassProtect_Click);
            // 
            // btnViewDecryptPdf
            // 
            this.btnViewDecryptPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewDecryptPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewDecryptPdf.Location = new System.Drawing.Point(668, 35);
            this.btnViewDecryptPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnViewDecryptPdf.Name = "btnViewDecryptPdf";
            this.btnViewDecryptPdf.Size = new System.Drawing.Size(106, 32);
            this.btnViewDecryptPdf.Symbol = 61550;
            this.btnViewDecryptPdf.TabIndex = 24;
            this.btnViewDecryptPdf.Text = "预览";
            this.btnViewDecryptPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewDecryptPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnViewDecryptPdf.Click += new System.EventHandler(this.btnViewDecryptPdf_Click);
            // 
            // btnDecryptPdf
            // 
            this.btnDecryptPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDecryptPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDecryptPdf.Location = new System.Drawing.Point(539, 35);
            this.btnDecryptPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnDecryptPdf.Name = "btnDecryptPdf";
            this.btnDecryptPdf.Size = new System.Drawing.Size(122, 32);
            this.btnDecryptPdf.Symbol = 61564;
            this.btnDecryptPdf.TabIndex = 23;
            this.btnDecryptPdf.Text = "选择文件";
            this.btnDecryptPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDecryptPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnDecryptPdf.Click += new System.EventHandler(this.btnDecryptPdf_Click);
            // 
            // tDecryptEditPass
            // 
            this.tDecryptEditPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tDecryptEditPass.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tDecryptEditPass.Location = new System.Drawing.Point(120, 74);
            this.tDecryptEditPass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tDecryptEditPass.MinimumSize = new System.Drawing.Size(1, 16);
            this.tDecryptEditPass.Name = "tDecryptEditPass";
            this.tDecryptEditPass.PasswordChar = '*';
            this.tDecryptEditPass.ShowText = false;
            this.tDecryptEditPass.Size = new System.Drawing.Size(202, 32);
            this.tDecryptEditPass.TabIndex = 22;
            this.tDecryptEditPass.TagString = "";
            this.tDecryptEditPass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tDecryptEditPass.Watermark = "编辑Pdf文件的密码";
            this.tDecryptEditPass.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tEncryptEditPass
            // 
            this.tEncryptEditPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tEncryptEditPass.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tEncryptEditPass.Location = new System.Drawing.Point(120, 77);
            this.tEncryptEditPass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tEncryptEditPass.MinimumSize = new System.Drawing.Size(1, 16);
            this.tEncryptEditPass.Name = "tEncryptEditPass";
            this.tEncryptEditPass.PasswordChar = '*';
            this.tEncryptEditPass.ShowText = false;
            this.tEncryptEditPass.Size = new System.Drawing.Size(202, 32);
            this.tEncryptEditPass.TabIndex = 15;
            this.tEncryptEditPass.TagString = "";
            this.tEncryptEditPass.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tEncryptEditPass.Watermark = "编辑Pdf文件的密码";
            this.tEncryptEditPass.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tEncryptPdfPath
            // 
            this.tEncryptPdfPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tEncryptPdfPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tEncryptPdfPath.Location = new System.Drawing.Point(120, 40);
            this.tEncryptPdfPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tEncryptPdfPath.MinimumSize = new System.Drawing.Size(1, 16);
            this.tEncryptPdfPath.Name = "tEncryptPdfPath";
            this.tEncryptPdfPath.ReadOnly = true;
            this.tEncryptPdfPath.ShowText = false;
            this.tEncryptPdfPath.Size = new System.Drawing.Size(397, 32);
            this.tEncryptPdfPath.TabIndex = 14;
            this.tEncryptPdfPath.TagString = "";
            this.tEncryptPdfPath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tEncryptPdfPath.Watermark = "";
            this.tEncryptPdfPath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel2.Location = new System.Drawing.Point(19, 77);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 27);
            this.uiLabel2.TabIndex = 13;
            this.uiLabel2.Text = "编辑密码：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel5.Location = new System.Drawing.Point(19, 74);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(120, 27);
            this.uiLabel5.TabIndex = 20;
            this.uiLabel5.Text = "编辑密码：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel5.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // groupBoxDecrypt
            // 
            this.groupBoxDecrypt.Controls.Add(this.btnRemovePassProtect);
            this.groupBoxDecrypt.Controls.Add(this.btnViewDecryptPdf);
            this.groupBoxDecrypt.Controls.Add(this.btnDecryptPdf);
            this.groupBoxDecrypt.Controls.Add(this.tDecryptEditPass);
            this.groupBoxDecrypt.Controls.Add(this.tDecryptPdf);
            this.groupBoxDecrypt.Controls.Add(this.uiLabel5);
            this.groupBoxDecrypt.Controls.Add(this.uiLabel6);
            this.groupBoxDecrypt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxDecrypt.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxDecrypt.Location = new System.Drawing.Point(10, 312);
            this.groupBoxDecrypt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxDecrypt.MinimumSize = new System.Drawing.Size(1, 1);
            this.groupBoxDecrypt.Name = "groupBoxDecrypt";
            this.groupBoxDecrypt.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.groupBoxDecrypt.Size = new System.Drawing.Size(780, 168);
            this.groupBoxDecrypt.TabIndex = 3;
            this.groupBoxDecrypt.Text = "解密";
            this.groupBoxDecrypt.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.groupBoxDecrypt.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(19, 41);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(107, 27);
            this.uiLabel1.TabIndex = 12;
            this.uiLabel1.Text = "文件路径:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // groupBoxEncrypt
            // 
            this.groupBoxEncrypt.Controls.Add(this.uiCheckBoxGroupMain);
            this.groupBoxEncrypt.Controls.Add(this.uiLabel4);
            this.groupBoxEncrypt.Controls.Add(this.tEncryptReadPass);
            this.groupBoxEncrypt.Controls.Add(this.uiLabel3);
            this.groupBoxEncrypt.Controls.Add(this.btnEncryptProtect);
            this.groupBoxEncrypt.Controls.Add(this.btnViewEncryptPdf);
            this.groupBoxEncrypt.Controls.Add(this.btnEncryptPdf);
            this.groupBoxEncrypt.Controls.Add(this.tEncryptEditPass);
            this.groupBoxEncrypt.Controls.Add(this.tEncryptPdfPath);
            this.groupBoxEncrypt.Controls.Add(this.uiLabel2);
            this.groupBoxEncrypt.Controls.Add(this.uiLabel1);
            this.groupBoxEncrypt.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxEncrypt.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBoxEncrypt.Location = new System.Drawing.Point(10, 35);
            this.groupBoxEncrypt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBoxEncrypt.MinimumSize = new System.Drawing.Size(1, 1);
            this.groupBoxEncrypt.Name = "groupBoxEncrypt";
            this.groupBoxEncrypt.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.groupBoxEncrypt.Size = new System.Drawing.Size(780, 277);
            this.groupBoxEncrypt.TabIndex = 2;
            this.groupBoxEncrypt.Text = "加密";
            this.groupBoxEncrypt.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.groupBoxEncrypt.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // PdfProtectForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 490);
            this.Controls.Add(this.groupBoxDecrypt);
            this.Controls.Add(this.groupBoxEncrypt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PdfProtectForm";
            this.Padding = new System.Windows.Forms.Padding(10, 35, 10, 10);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "加密/解密";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.Load += new System.EventHandler(this.PdfProtectForm_Load);
            this.groupBoxDecrypt.ResumeLayout(false);
            this.groupBoxEncrypt.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UITextBox tDecryptPdf;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UICheckBoxGroup uiCheckBoxGroupMain;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox tEncryptReadPass;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIButton btnEncryptProtect;
        private Sunny.UI.UISymbolButton btnViewEncryptPdf;
        private Sunny.UI.UISymbolButton btnEncryptPdf;
        private Sunny.UI.UIButton btnRemovePassProtect;
        private Sunny.UI.UISymbolButton btnViewDecryptPdf;
        private Sunny.UI.UISymbolButton btnDecryptPdf;
        private Sunny.UI.UITextBox tDecryptEditPass;
        private Sunny.UI.UITextBox tEncryptEditPass;
        private Sunny.UI.UITextBox tEncryptPdfPath;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIGroupBox groupBoxDecrypt;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIGroupBox groupBoxEncrypt;
    }
}