﻿using Base.Platform;
using FileKit.Common;
using iText.Kernel.Pdf;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Pdf
{
    public partial class PdfProtectForm : UIForm
    {
        public PdfProtectForm(Module module)
        {
            InitializeComponent();
        }

        private void PdfProtectForm_Load(object sender, EventArgs e)
        {
            List<Permission> permissions = new List<Permission>();
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_COPY, Name = "允许内容复制" });
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_MODIFY_CONTENTS, Name = "允许用户修改内容" });
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_PRINTING, Name = "文档允许打印" });
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_SCREENREADERS, Name = "启用内容辅助功能" });
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_MODIFY_ANNOTATIONS, Name = "允许修改注释" });
            permissions.Add(new Permission() { ID = EncryptionConstants.ALLOW_FILL_IN, Name = "允许用户填写表单字段" });
            uiCheckBoxGroupMain.Items.AddRange(permissions.ToArray());
        }

        private void btnEncryptPdf_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Pdf文件|*.pdf" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tEncryptPdfPath.Text = openFileDialog.FileName;
                }
            }
        }

        private void btnViewEncryptPdf_Click(object sender, EventArgs e)
        {
            this.showPdfView(tEncryptPdfPath.Text);
        }

        private void showPdfView(string path, string title = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Utils.ShowPdf(path);
            //PdfView pdfView = new PdfView(path, title);
            //pdfView.Show();
        }

        private void btnEncryptProtect_Click(object sender, EventArgs e)
        {
            string pdfPath = tEncryptPdfPath.Text;
            if (string.IsNullOrEmpty(pdfPath))
            {
                ShowWarningDialog("提示", "请选择待加密文件！");
                // MessageBox.Show("请选择待加密文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(this.tEncryptEditPass.Text))
            {
                ShowWarningDialog("提示", "请输入编辑密码！");
                //MessageBox.Show("请输入编辑密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            int encryption = 0;
            if (uiCheckBoxGroupMain.SelectedItems.Count != 0)
            {
                foreach (object item in uiCheckBoxGroupMain.SelectedItems)
                {
                    encryption |= ((Permission)item).ID;
                }
            }
            PdfWriter pdfWriter = null;
            PdfReader pdfReader = null;
            PdfDocument pdfDocument = null;
            try
            {
                pdfReader = new PdfReader(pdfPath);
                WriterProperties writerProperties = new WriterProperties();

                byte[] USERPASS = Encoding.Default.GetBytes(this.tEncryptReadPass.Text);
                byte[] OWNERPASS = Encoding.Default.GetBytes(this.tEncryptEditPass.Text);

                writerProperties.SetStandardEncryption(USERPASS, OWNERPASS, encryption, EncryptionConstants.ENCRYPTION_AES_256);
                pdfWriter = new PdfWriter(new FileStream(string.Format("{0}/{1}_protected.pdf", System.IO.Path.GetDirectoryName(pdfPath), System.IO.Path.GetFileNameWithoutExtension(pdfPath)), FileMode.Create), writerProperties);
                pdfDocument = new PdfDocument(pdfReader, pdfWriter);
                pdfDocument.Close();
                ShowSuccessDialog("提示", "处理完成！");
                //MessageBox.Show("处理完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                ShowErrorDialog("异常", ex.Message);
                //MessageBox.Show(ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (pdfWriter != null)
                    pdfWriter.Close();
                if (pdfReader != null)
                    pdfReader.Close();
            }
        }

        private void btnDecryptPdf_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Pdf文件|*.pdf" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tDecryptPdf.Text = openFileDialog.FileName;
                }
            }
        }

        private void btnViewDecryptPdf_Click(object sender, EventArgs e)
        {
            this.showPdfView(tDecryptPdf.Text);
        }

        private void btnRemovePassProtect_Click(object sender, EventArgs e)
        {
            string pdfPath = tDecryptPdf.Text;
            if (string.IsNullOrEmpty(pdfPath))
            {
                ShowWarningDialog("提示", "请选择待移除密码的Pdf文件！");
                //MessageBox.Show("请选择待移除密码的Pdf文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(this.tDecryptEditPass.Text))
            {
                ShowWarningDialog("提示", "请输入编辑密码！");
                //MessageBox.Show("请输入编辑密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            PdfWriter pdfWriter = null;
            PdfReader pdfReader = null;
            PdfDocument pdfDocument = null;
            try
            {
                byte[] OWNERPASS = Encoding.Default.GetBytes(this.tDecryptEditPass.Text);

                ReaderProperties readerProperties = new ReaderProperties().SetPassword(OWNERPASS);
                pdfReader = new PdfReader(pdfPath, readerProperties);
                pdfWriter = new PdfWriter(new FileStream(string.Format("{0}/{1}_unlocked.pdf", System.IO.Path.GetDirectoryName(pdfPath), System.IO.Path.GetFileNameWithoutExtension(pdfPath)), FileMode.Create));
                pdfDocument = new PdfDocument(pdfReader, pdfWriter);
                pdfDocument.Close();
                ShowSuccessDialog("提示", "处理完成！");
                //MessageBox.Show("处理完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                ShowErrorDialog("异常", ex.Message);
                //MessageBox.Show(ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (pdfWriter != null)
                    pdfWriter.Close();
                if (pdfReader != null)
                    pdfReader.Close();
            }
        }
    }
}
