﻿using Base.Platform;
using FileKit.Common;
using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using iText.Layout;
using iText.Layout.Borders;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Pdf
{
    public partial class PdfSplitForm : UIForm
    {
        public PdfSplitForm(Module module)
        {
            InitializeComponent();
        }

        private void fnSelectSplitPdf()
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Pdf文件|*.pdf" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tPdfPathSplt.Text = openFileDialog.FileName;
                }
            }
        }

        private void btnSelectSplitPdf_Click(object sender, EventArgs e)
        {
            fnSelectSplitPdf();
        }

        private void btnViewSplitPdf_Click(object sender, EventArgs e)
        {
            this.showPdfView(tPdfPathSplt.Text);
        }

        private void chbSplitAll_CheckedChanged(object sender, EventArgs e)
        {
            tPdfPages.Enabled = !chbSplitAll.Checked;
        }

        private void btnSplitPdf_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerMain.IsBusy)
            {
                ShowWarningDialog("警告", "正忙，请稍候...");
                //MessageBox.Show("正忙，请稍候...", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string pdfPath = tPdfPathSplt.Text;
            string numPages = tPdfPages.Text;

            if (string.IsNullOrEmpty(pdfPath) || (string.IsNullOrEmpty(numPages) && !chbSplitAll.Checked))
            {
                ShowWarningDialog("提示", "请选择待分拆Pdf文件，并按格式录入页码范围或选择逐页分拆！");
                //MessageBox.Show("请选择待分拆Pdf文件，并按格式录入页码范围或选择逐页分拆！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!File.Exists(pdfPath))
            {
                ShowErrorDialog("提示", "当前文件不存在，请重新选择！");
                //MessageBox.Show("当前文件不存在，请重新选择！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string pdfDir = System.IO.Path.GetDirectoryName(pdfPath);
            List<Common.PageRange> pageNums = new List<Common.PageRange>();

            if (!chbSplitAll.Checked)
            {
                string[] pages = numPages.Split(',');
                try
                {
                    foreach (string page in pages)
                    {
                        int num = -1;
                        if (int.TryParse(page, out num))
                        {
                            pageNums.Add(new Common.PageRange(num));
                            continue;
                        }
                        else if (page.Contains("-"))
                        {
                            string[] subPages = page.Split('-');
                            if (subPages.Length != 2)
                            {
                                throw new Exception("");
                            }
                            int start = int.Parse(subPages[0]);
                            int end = int.Parse(subPages[1]);

                            pageNums.Add(new Common.PageRange(start, end));
                            continue;
                        }
                        throw new Exception("");
                    }
                }
                catch (Exception)
                {
                    ShowErrorDialog("提示", "页码范围有误，请重新输入！");
                    //MessageBox.Show("页码范围有误，请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            uiGroupBoxMain.Enabled = false;
            lbSplitMsg.Text = "开始处理...";
            lbSplitMsg.Visible = true;
            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                PdfDocument pdfDocument = new PdfDocument(new PdfReader(new FileStream(pdfPath, FileMode.Open, FileAccess.Read, FileShare.Read)));
                int totalPages = pdfDocument.GetNumberOfPages();
                if (chbSplitAll.Checked)
                {
                    for (int index = 1; index <= totalPages; index++)
                        pageNums.Add(new Common.PageRange(index));
                }

                string dirPath = string.Format("{0}\\{1}", pdfDir, System.IO.Path.GetFileNameWithoutExtension(pdfPath));
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                bool autoAddMerge = chAutoChecked.Checked;
                foreach (Common.PageRange pageRange in pageNums)
                {
                    int start = pageRange.start;
                    int end = Math.Min(pageRange.end, totalPages);

                    if (start > end)
                        continue;

                    string pdf = string.Format("{0}\\{1}\\{2}.pdf", pdfDir, System.IO.Path.GetFileNameWithoutExtension(pdfPath), pageRange.ToString());
                    if (File.Exists(pdf))
                        File.Delete(pdf);
                    Base.Common.Utils.Invoke(lbSplitMsg, () =>
                    {
                        lbSplitMsg.Text = string.Format("正在处理 {0}", System.IO.Path.GetFileName(pdf));
                        if (autoAddMerge)
                            uiListBoxMain.Items.Add(pdf);
                    });

                    PdfWriter pdfWriter = new PdfWriter(pdf);
                    PdfDocument pdfWriterDoc = new PdfDocument(pdfWriter);
                    pdfDocument.CopyPagesTo(start, end, pdfWriterDoc);
                    pdfWriterDoc.Close();
                    pdfWriter.Close();
                }

                pdfDocument.Close();
            }, (s3, e3) =>
           {
               lbSplitMsg.Text = "处理完成！";
               ShowSuccessDialog("提示", "处理完成!");
               //MessageBox.Show("处理完成！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
               lbSplitMsg.Visible = false;
               uiGroupBoxMain.Enabled = true;
           });
        }

        private void btnMergeAdd_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Multiselect = true, Filter = "文件|*.pdf;*.gif;*.jpg;*.jpeg;*.bmp;*.jfif;*.png;|PDF文件|*.pdf|图片文件|*.gif;*.jpg;*.jpeg;*.bmp;*.jfif;*.png;" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    foreach (string file in openFileDialog.FileNames)
                    {
                        uiListBoxMain.Items.Add(file);
                    }
                }
            }
        }

        private void btnMergeDel_Click(object sender, EventArgs e)
        {
            int index = uiListBoxMain.SelectedIndex;
            if (index == -1)
            {
                ShowWarningDialog("提示", "请选择一项！");
                //MessageBox.Show("请选择一项！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            uiListBoxMain.Items.RemoveAt(uiListBoxMain.SelectedIndex);
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            object item = uiListBoxMain.SelectedItem;
            int index = uiListBoxMain.SelectedIndex;
            if (index == -1)
            {
                ShowWarningDialog("提示", "请选择一项！");
                //MessageBox.Show("请选择一项！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (index == 0)
            {
                ShowWarningDialog("提示", "已在首位！");
                // MessageBox.Show("已在首位！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            uiListBoxMain.Items.Remove(item);
            uiListBoxMain.Items.Insert(index - 1, item);
            uiListBoxMain.SelectedIndex = index - 1;
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            object item = uiListBoxMain.SelectedItem;
            int index = uiListBoxMain.SelectedIndex;
            if (index == -1)
            {
                ShowWarningDialog("提示", "请选择一项！");
                //MessageBox.Show("请选择一项！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (index == uiListBoxMain.Items.Count - 1)
            {
                ShowWarningDialog("提示", "已在末尾！");
                //MessageBox.Show("已在末尾！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            uiListBoxMain.Items.Remove(item);
            uiListBoxMain.Items.Insert(index + 1, item);
            uiListBoxMain.SelectedIndex = index + 1;
        }

        private void btnMergeClear_Click(object sender, EventArgs e)
        {
            uiListBoxMain.Items.Clear();
        }

        private void btnMergePdf_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerMain.IsBusy)
            {
                ShowWarningDialog("提示", "正忙，请稍候...");
                //MessageBox.Show("正忙，请稍候...", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (uiListBoxMain.Items.Count < 1)
            {
                ShowWarningDialog("提示", "请添加至少1个文件！");
                //MessageBox.Show("请添加至少1个文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string savePath = System.IO.Path.GetTempPath();
            lbMergeMsg.Text = "开始合并文件...";
            lbMergeMsg.Visible = true;
            uiGroupBoxMerge.Enabled = false;
            string mergedPdf = string.Format("{0}{1}.pdf", savePath, Guid.NewGuid());
            if (File.Exists(mergedPdf))
                File.Delete(mergedPdf);

            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                PdfWriter pdfWriter = new PdfWriter(mergedPdf);
                PdfDocument pdfWriterDoc = new PdfDocument(pdfWriter);

                PdfMerger merger = new PdfMerger(pdfWriterDoc);
                foreach (object item in uiListBoxMain.Items)
                {
                    Base.Common.Utils.Invoke(lbSplitMsg, () =>
                    {
                        lbMergeMsg.Text = string.Format("正在处理 {0}", System.IO.Path.GetFileName(item.ToString()));
                    });
                    PdfDocument pdfDoc = null;
                    if (System.IO.Path.GetExtension(item.ToString()).ToUpper() != ".PDF")
                    {
                        string tempPdf = imageToPdf(item.ToString());
                        pdfDoc = new PdfDocument(new PdfReader(tempPdf));
                    }
                    else
                        pdfDoc = new PdfDocument(new PdfReader(item.ToString()));
                    merger.Merge(pdfDoc, 1, pdfDoc.GetNumberOfPages());
                    pdfDoc.Close();
                }
                pdfWriterDoc.Close();
            }, (s2, e2) =>
            {
                lbMergeMsg.Text = "处理完成!";
                uiGroupBoxMerge.Enabled = true;

                lbMergeMsg.Visible = false;

                this.showPdfView(mergedPdf, "预览合并后文件（请点击下列的图标进行操作！）");
            });
        }


        private string imageToPdf(string imagePdf)
        {
            string pdfName = string.Format("{0}{1}.pdf", System.IO.Path.GetTempPath(), System.IO.Path.GetFileNameWithoutExtension(imagePdf));
            PdfWriter writer = new PdfWriter(pdfName);
            PdfDocument pdfDoc = new PdfDocument(writer);
            ImageData data = ImageDataFactory.Create(imagePdf);
            iText.Layout.Element.Image img = new iText.Layout.Element.Image(data);
            img.SetBorder(Border.NO_BORDER);
            float imgWidth = img.GetImageWidth();
            float imgHeight = img.GetImageHeight();
            img.SetFixedPosition(0, 0);
            iText.Kernel.Geom.Rectangle rect = new iText.Kernel.Geom.Rectangle(imgWidth, imgHeight);
            PageSize pagesize = new PageSize(rect);
            pdfDoc.SetDefaultPageSize(pagesize);
            Document document = new Document(pdfDoc, pagesize);
            document.SetMargins(0, 0, 0, 0);
            document.Add(img);

            document.Close();

            return pdfName;
        }

        private void showPdfView(string path, string title = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (File.Exists(path))
                Utils.ShowPdf(path);
            else
                ShowErrorDialog("异常", "文件不存在！");
            //PdfView pdfView = new PdfView(path, title);
            //pdfView.Show();
        }

        private void uiListBoxMain_ItemDoubleClick(object sender, EventArgs e)
        {
            this.showFile(uiListBoxMain.SelectedItem.ToString());
        }

        private void showFile(string filePath)
        {
            string extension = System.IO.Path.GetExtension(filePath) + "";
            if (extension.ToUpper() == ".PDF")
                this.showPdfView(filePath);
            else
                Utils.ShowImage(filePath);
        }

        private void uiListBoxMain_MouseMove(object sender, MouseEventArgs e)
        {
            int index = uiListBoxMain.IndexFromPoint(e.Location);
            if (index != -1 && index < uiListBoxMain.Items.Count)
            {
                if (uiToolTipMain.GetToolTip(uiListBoxMain) != uiListBoxMain.Items[index].ToString())
                {
                    uiToolTipMain.SetToolTip(uiListBoxMain, uiListBoxMain.Items[index].ToString());
                }
            }
        }

    }
}
