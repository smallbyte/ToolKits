﻿namespace FileKit.Pages.Pdf
{
    partial class PdfSplitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PdfSplitForm));
            this.btnMergeDel = new Sunny.UI.UIButton();
            this.btnSplitPdf = new Sunny.UI.UIButton();
            this.lbSplitMsg = new Sunny.UI.UILabel();
            this.chAutoChecked = new Sunny.UI.UICheckBox();
            this.chbSplitAll = new Sunny.UI.UICheckBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.btnViewSplitPdf = new Sunny.UI.UISymbolButton();
            this.lbMergeMsg = new Sunny.UI.UILabel();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.uiListBoxMain = new Sunny.UI.UIListBox();
            this.btnSelectSplitPdf = new Sunny.UI.UISymbolButton();
            this.tPdfPages = new Sunny.UI.UITextBox();
            this.tPdfPathSplt = new Sunny.UI.UITextBox();
            this.btnMergeAdd = new Sunny.UI.UIButton();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiToolTipMain = new Sunny.UI.UIToolTip(this.components);
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.btnMergePdf = new Sunny.UI.UIButton();
            this.btnMergeClear = new Sunny.UI.UIButton();
            this.btnMoveDown = new Sunny.UI.UIButton();
            this.uiGroupBoxMerge = new Sunny.UI.UIGroupBox();
            this.btnMoveUp = new Sunny.UI.UIButton();
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.uiGroupBox1.SuspendLayout();
            this.uiGroupBoxMerge.SuspendLayout();
            this.uiGroupBoxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMergeDel
            // 
            this.btnMergeDel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMergeDel.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnMergeDel.Location = new System.Drawing.Point(573, 89);
            this.btnMergeDel.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMergeDel.Name = "btnMergeDel";
            this.btnMergeDel.Size = new System.Drawing.Size(73, 32);
            this.btnMergeDel.TabIndex = 14;
            this.btnMergeDel.Text = "删除";
            this.btnMergeDel.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMergeDel.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMergeDel.Click += new System.EventHandler(this.btnMergeDel_Click);
            // 
            // btnSplitPdf
            // 
            this.btnSplitPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSplitPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSplitPdf.Location = new System.Drawing.Point(530, 84);
            this.btnSplitPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSplitPdf.Name = "btnSplitPdf";
            this.btnSplitPdf.Size = new System.Drawing.Size(244, 57);
            this.btnSplitPdf.TabIndex = 11;
            this.btnSplitPdf.Text = "提取";
            this.btnSplitPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSplitPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSplitPdf.Click += new System.EventHandler(this.btnSplitPdf_Click);
            // 
            // lbSplitMsg
            // 
            this.lbSplitMsg.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbSplitMsg.ForeColor = System.Drawing.Color.Red;
            this.lbSplitMsg.Location = new System.Drawing.Point(39, 179);
            this.lbSplitMsg.Name = "lbSplitMsg";
            this.lbSplitMsg.Size = new System.Drawing.Size(287, 23);
            this.lbSplitMsg.Style = Sunny.UI.UIStyle.Custom;
            this.lbSplitMsg.StyleCustomMode = true;
            this.lbSplitMsg.TabIndex = 10;
            this.lbSplitMsg.Text = "...";
            this.lbSplitMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbSplitMsg.Visible = false;
            this.lbSplitMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // chAutoChecked
            // 
            this.chAutoChecked.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chAutoChecked.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chAutoChecked.Location = new System.Drawing.Point(254, 147);
            this.chAutoChecked.MinimumSize = new System.Drawing.Size(1, 1);
            this.chAutoChecked.Name = "chAutoChecked";
            this.chAutoChecked.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.chAutoChecked.Size = new System.Drawing.Size(246, 29);
            this.chAutoChecked.TabIndex = 9;
            this.chAutoChecked.Text = "自动添加到待合并列表";
            this.chAutoChecked.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // chbSplitAll
            // 
            this.chbSplitAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chbSplitAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.chbSplitAll.Location = new System.Drawing.Point(124, 147);
            this.chbSplitAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.chbSplitAll.Name = "chbSplitAll";
            this.chbSplitAll.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.chbSplitAll.Size = new System.Drawing.Size(150, 29);
            this.chbSplitAll.TabIndex = 8;
            this.chbSplitAll.Text = "逐页分拆";
            this.chbSplitAll.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.chbSplitAll.CheckedChanged += new System.EventHandler(this.chbSplitAll_CheckedChanged);
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.uiLabel4.ForeColor = System.Drawing.Color.Blue;
            this.uiLabel4.Location = new System.Drawing.Point(250, 119);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(217, 24);
            this.uiLabel4.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel4.StyleCustomMode = true;
            this.uiLabel4.TabIndex = 7;
            this.uiLabel4.Text = "(分拆后会生成对应数量的文件)";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel4.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.uiLabel3.ForeColor = System.Drawing.Color.Red;
            this.uiLabel3.Location = new System.Drawing.Point(122, 118);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(128, 26);
            this.uiLabel3.Style = Sunny.UI.UIStyle.Custom;
            this.uiLabel3.StyleCustomMode = true;
            this.uiLabel3.TabIndex = 6;
            this.uiLabel3.Text = "例如：1,5,10-20";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel3.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnViewSplitPdf
            // 
            this.btnViewSplitPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewSplitPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewSplitPdf.Location = new System.Drawing.Point(658, 46);
            this.btnViewSplitPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnViewSplitPdf.Name = "btnViewSplitPdf";
            this.btnViewSplitPdf.Size = new System.Drawing.Size(116, 32);
            this.btnViewSplitPdf.Symbol = 61550;
            this.btnViewSplitPdf.TabIndex = 5;
            this.btnViewSplitPdf.Text = "预览";
            this.btnViewSplitPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewSplitPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnViewSplitPdf.Click += new System.EventHandler(this.btnViewSplitPdf_Click);
            // 
            // lbMergeMsg
            // 
            this.lbMergeMsg.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbMergeMsg.ForeColor = System.Drawing.Color.Red;
            this.lbMergeMsg.Location = new System.Drawing.Point(28, 244);
            this.lbMergeMsg.Name = "lbMergeMsg";
            this.lbMergeMsg.Size = new System.Drawing.Size(292, 23);
            this.lbMergeMsg.Style = Sunny.UI.UIStyle.Custom;
            this.lbMergeMsg.StyleCustomMode = true;
            this.lbMergeMsg.TabIndex = 12;
            this.lbMergeMsg.Text = "...";
            this.lbMergeMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMergeMsg.Visible = false;
            this.lbMergeMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.uiListBoxMain);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.uiGroupBox1.Location = new System.Drawing.Point(31, 34);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(2, 32, 2, 2);
            this.uiGroupBox1.Size = new System.Drawing.Size(528, 205);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "待合并文件列表（支持图片和pdf文件，双击列表项预览pdf文件）";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiListBoxMain
            // 
            this.uiListBoxMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiListBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBoxMain.FillColor = System.Drawing.Color.White;
            this.uiListBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiListBoxMain.Location = new System.Drawing.Point(2, 32);
            this.uiListBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiListBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiListBoxMain.Name = "uiListBoxMain";
            this.uiListBoxMain.Padding = new System.Windows.Forms.Padding(2);
            this.uiListBoxMain.ShowText = false;
            this.uiListBoxMain.Size = new System.Drawing.Size(524, 171);
            this.uiListBoxMain.TabIndex = 0;
            this.uiListBoxMain.Text = "uiListBox1";
            this.uiListBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiListBoxMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.uiListBoxMain_MouseMove);
            this.uiListBoxMain.DoubleClick += new System.EventHandler(this.uiListBoxMain_ItemDoubleClick);
            // 
            // btnSelectSplitPdf
            // 
            this.btnSelectSplitPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectSplitPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectSplitPdf.Location = new System.Drawing.Point(530, 46);
            this.btnSelectSplitPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelectSplitPdf.Name = "btnSelectSplitPdf";
            this.btnSelectSplitPdf.Size = new System.Drawing.Size(122, 32);
            this.btnSelectSplitPdf.Symbol = 61564;
            this.btnSelectSplitPdf.TabIndex = 4;
            this.btnSelectSplitPdf.Text = "选择文件";
            this.btnSelectSplitPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectSplitPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSelectSplitPdf.Click += new System.EventHandler(this.btnSelectSplitPdf_Click);
            // 
            // tPdfPages
            // 
            this.tPdfPages.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPdfPages.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tPdfPages.Location = new System.Drawing.Point(126, 84);
            this.tPdfPages.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPdfPages.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPdfPages.Name = "tPdfPages";
            this.tPdfPages.ShowText = false;
            this.tPdfPages.Size = new System.Drawing.Size(397, 32);
            this.tPdfPages.TabIndex = 3;
            this.tPdfPages.TagString = "";
            this.tPdfPages.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPdfPages.Watermark = "";
            this.tPdfPages.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tPdfPathSplt
            // 
            this.tPdfPathSplt.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPdfPathSplt.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tPdfPathSplt.Location = new System.Drawing.Point(126, 46);
            this.tPdfPathSplt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPdfPathSplt.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPdfPathSplt.Name = "tPdfPathSplt";
            this.tPdfPathSplt.ReadOnly = true;
            this.tPdfPathSplt.ShowText = false;
            this.tPdfPathSplt.Size = new System.Drawing.Size(397, 32);
            this.tPdfPathSplt.TabIndex = 2;
            this.tPdfPathSplt.TagString = "";
            this.tPdfPathSplt.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPdfPathSplt.Watermark = "";
            this.tPdfPathSplt.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnMergeAdd
            // 
            this.btnMergeAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMergeAdd.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnMergeAdd.Location = new System.Drawing.Point(573, 51);
            this.btnMergeAdd.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMergeAdd.Name = "btnMergeAdd";
            this.btnMergeAdd.Size = new System.Drawing.Size(73, 32);
            this.btnMergeAdd.TabIndex = 13;
            this.btnMergeAdd.Text = "添加";
            this.btnMergeAdd.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMergeAdd.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMergeAdd.Click += new System.EventHandler(this.btnMergeAdd_Click);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel2.Location = new System.Drawing.Point(26, 83);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 27);
            this.uiLabel2.TabIndex = 1;
            this.uiLabel2.Text = "页面范围：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiToolTipMain
            // 
            this.uiToolTipMain.BackColor = System.Drawing.Color.White;
            this.uiToolTipMain.ForeColor = System.Drawing.Color.Black;
            this.uiToolTipMain.OwnerDraw = true;
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(26, 46);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(107, 27);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "文件路径:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnMergePdf
            // 
            this.btnMergePdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMergePdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMergePdf.Location = new System.Drawing.Point(652, 51);
            this.btnMergePdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMergePdf.Name = "btnMergePdf";
            this.btnMergePdf.Size = new System.Drawing.Size(121, 70);
            this.btnMergePdf.TabIndex = 18;
            this.btnMergePdf.Text = "合并";
            this.btnMergePdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMergePdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMergePdf.Click += new System.EventHandler(this.btnMergePdf_Click);
            // 
            // btnMergeClear
            // 
            this.btnMergeClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMergeClear.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnMergeClear.Location = new System.Drawing.Point(573, 203);
            this.btnMergeClear.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMergeClear.Name = "btnMergeClear";
            this.btnMergeClear.Size = new System.Drawing.Size(73, 32);
            this.btnMergeClear.TabIndex = 17;
            this.btnMergeClear.Text = "清空";
            this.btnMergeClear.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMergeClear.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMergeClear.Click += new System.EventHandler(this.btnMergeClear_Click);
            // 
            // btnMoveDown
            // 
            this.btnMoveDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMoveDown.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnMoveDown.Location = new System.Drawing.Point(573, 165);
            this.btnMoveDown.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMoveDown.Name = "btnMoveDown";
            this.btnMoveDown.Size = new System.Drawing.Size(73, 32);
            this.btnMoveDown.TabIndex = 16;
            this.btnMoveDown.Text = "下移";
            this.btnMoveDown.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMoveDown.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMoveDown.Click += new System.EventHandler(this.btnMoveDown_Click);
            // 
            // uiGroupBoxMerge
            // 
            this.uiGroupBoxMerge.Controls.Add(this.btnMergePdf);
            this.uiGroupBoxMerge.Controls.Add(this.btnMergeClear);
            this.uiGroupBoxMerge.Controls.Add(this.btnMoveDown);
            this.uiGroupBoxMerge.Controls.Add(this.btnMoveUp);
            this.uiGroupBoxMerge.Controls.Add(this.btnMergeDel);
            this.uiGroupBoxMerge.Controls.Add(this.btnMergeAdd);
            this.uiGroupBoxMerge.Controls.Add(this.lbMergeMsg);
            this.uiGroupBoxMerge.Controls.Add(this.uiGroupBox1);
            this.uiGroupBoxMerge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxMerge.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMerge.Location = new System.Drawing.Point(10, 256);
            this.uiGroupBoxMerge.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMerge.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMerge.Name = "uiGroupBoxMerge";
            this.uiGroupBoxMerge.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBoxMerge.Size = new System.Drawing.Size(780, 278);
            this.uiGroupBoxMerge.TabIndex = 3;
            this.uiGroupBoxMerge.Text = "合并";
            this.uiGroupBoxMerge.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMerge.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnMoveUp
            // 
            this.btnMoveUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMoveUp.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnMoveUp.Location = new System.Drawing.Point(573, 127);
            this.btnMoveUp.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnMoveUp.Name = "btnMoveUp";
            this.btnMoveUp.Size = new System.Drawing.Size(73, 32);
            this.btnMoveUp.TabIndex = 15;
            this.btnMoveUp.Text = "上移";
            this.btnMoveUp.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnMoveUp.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnMoveUp.Click += new System.EventHandler(this.btnMoveUp_Click);
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.btnSplitPdf);
            this.uiGroupBoxMain.Controls.Add(this.lbSplitMsg);
            this.uiGroupBoxMain.Controls.Add(this.chAutoChecked);
            this.uiGroupBoxMain.Controls.Add(this.chbSplitAll);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel4);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel3);
            this.uiGroupBoxMain.Controls.Add(this.btnViewSplitPdf);
            this.uiGroupBoxMain.Controls.Add(this.btnSelectSplitPdf);
            this.uiGroupBoxMain.Controls.Add(this.tPdfPages);
            this.uiGroupBoxMain.Controls.Add(this.tPdfPathSplt);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel2);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel1);
            this.uiGroupBoxMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMain.Location = new System.Drawing.Point(10, 35);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(780, 221);
            this.uiGroupBoxMain.TabIndex = 2;
            this.uiGroupBoxMain.Text = "分拆";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // PdfSplitForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 544);
            this.Controls.Add(this.uiGroupBoxMerge);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PdfSplitForm";
            this.Padding = new System.Windows.Forms.Padding(10, 35, 10, 10);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "分拆/合并";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBoxMerge.ResumeLayout(false);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIButton btnMergeDel;
        private Sunny.UI.UIButton btnSplitPdf;
        private Sunny.UI.UILabel lbSplitMsg;
        private Sunny.UI.UICheckBox chAutoChecked;
        private Sunny.UI.UICheckBox chbSplitAll;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UISymbolButton btnViewSplitPdf;
        private Sunny.UI.UILabel lbMergeMsg;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIListBox uiListBoxMain;
        private Sunny.UI.UISymbolButton btnSelectSplitPdf;
        private Sunny.UI.UITextBox tPdfPages;
        private Sunny.UI.UITextBox tPdfPathSplt;
        private Sunny.UI.UIButton btnMergeAdd;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIToolTip uiToolTipMain;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIButton btnMergePdf;
        private Sunny.UI.UIButton btnMergeClear;
        private Sunny.UI.UIButton btnMoveDown;
        private Sunny.UI.UIGroupBox uiGroupBoxMerge;
        private Sunny.UI.UIButton btnMoveUp;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private Sunny.UI.UIGroupBox uiGroupBoxMain;
    }
}