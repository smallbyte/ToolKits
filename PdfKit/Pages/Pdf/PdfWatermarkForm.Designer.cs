﻿namespace FileKit.Pages.Pdf
{
    partial class PdfWatermarkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PdfWatermarkForm));
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.uiPanelFont = new Sunny.UI.UIPanel();
            this.numFontSize = new Sunny.UI.UIIntegerUpDown();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.numAngle = new Sunny.UI.UIIntegerUpDown();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.tWatermark = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.rbImage = new Sunny.UI.UIRadioButton();
            this.rbFont = new Sunny.UI.UIRadioButton();
            this.btnDoWatermark = new Sunny.UI.UIButton();
            this.btnWaterViewPdf = new Sunny.UI.UISymbolButton();
            this.btnSelectWatermarkPdf = new Sunny.UI.UISymbolButton();
            this.tWatermarkPdf = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiGroupBoxWatermark = new Sunny.UI.UIGroupBox();
            this.uiPanelImage = new Sunny.UI.UIPanel();
            this.uiDoubleUpDownScale = new Sunny.UI.UIDoubleUpDown();
            this.uiLabel9 = new Sunny.UI.UILabel();
            this.uiIntegerUpDownRadAngle = new Sunny.UI.UIIntegerUpDown();
            this.uiIntegerUpDownBottom = new Sunny.UI.UIIntegerUpDown();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.uiLabel8 = new Sunny.UI.UILabel();
            this.uiIntegerUpDownLeft = new Sunny.UI.UIIntegerUpDown();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.uiTextBoxImagePath = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiPanelFont.SuspendLayout();
            this.uiGroupBoxWatermark.SuspendLayout();
            this.uiPanelImage.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanelFont
            // 
            this.uiPanelFont.Controls.Add(this.numFontSize);
            this.uiPanelFont.Controls.Add(this.uiLabel4);
            this.uiPanelFont.Controls.Add(this.numAngle);
            this.uiPanelFont.Controls.Add(this.uiLabel3);
            this.uiPanelFont.Controls.Add(this.tWatermark);
            this.uiPanelFont.Controls.Add(this.uiLabel2);
            this.uiPanelFont.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanelFont.Location = new System.Drawing.Point(34, 119);
            this.uiPanelFont.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanelFont.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanelFont.Name = "uiPanelFont";
            this.uiPanelFont.Size = new System.Drawing.Size(621, 146);
            this.uiPanelFont.TabIndex = 27;
            this.uiPanelFont.Text = null;
            this.uiPanelFont.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPanelFont.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // numFontSize
            // 
            this.numFontSize.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.numFontSize.Location = new System.Drawing.Point(106, 90);
            this.numFontSize.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numFontSize.Maximum = 90;
            this.numFontSize.Minimum = -90;
            this.numFontSize.MinimumSize = new System.Drawing.Size(100, 0);
            this.numFontSize.Name = "numFontSize";
            this.numFontSize.ShowText = false;
            this.numFontSize.Size = new System.Drawing.Size(152, 29);
            this.numFontSize.Step = 5;
            this.numFontSize.TabIndex = 30;
            this.numFontSize.Text = null;
            this.numFontSize.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.numFontSize.Value = 24;
            this.numFontSize.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel4.Location = new System.Drawing.Point(15, 90);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(100, 23);
            this.uiLabel4.TabIndex = 29;
            this.uiLabel4.Text = "字体大小:";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel4.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // numAngle
            // 
            this.numAngle.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.numAngle.Location = new System.Drawing.Point(106, 53);
            this.numAngle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.numAngle.Maximum = 90;
            this.numAngle.Minimum = -90;
            this.numAngle.MinimumSize = new System.Drawing.Size(100, 0);
            this.numAngle.Name = "numAngle";
            this.numAngle.ShowText = false;
            this.numAngle.Size = new System.Drawing.Size(152, 29);
            this.numAngle.Step = 5;
            this.numAngle.TabIndex = 28;
            this.numAngle.Text = null;
            this.numAngle.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.numAngle.Value = 45;
            this.numAngle.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel3.Location = new System.Drawing.Point(15, 55);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(100, 23);
            this.uiLabel3.TabIndex = 27;
            this.uiLabel3.Text = "旋转角度:";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel3.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tWatermark
            // 
            this.tWatermark.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tWatermark.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tWatermark.Location = new System.Drawing.Point(106, 16);
            this.tWatermark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tWatermark.MinimumSize = new System.Drawing.Size(1, 16);
            this.tWatermark.Name = "tWatermark";
            this.tWatermark.ShowText = false;
            this.tWatermark.Size = new System.Drawing.Size(362, 32);
            this.tWatermark.TabIndex = 26;
            this.tWatermark.TagString = "";
            this.tWatermark.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tWatermark.Watermark = "";
            this.tWatermark.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel2.Location = new System.Drawing.Point(15, 17);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(120, 27);
            this.uiLabel2.TabIndex = 25;
            this.uiLabel2.Text = "水印内容:";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // rbImage
            // 
            this.rbImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbImage.Location = new System.Drawing.Point(190, 82);
            this.rbImage.MinimumSize = new System.Drawing.Size(1, 1);
            this.rbImage.Name = "rbImage";
            this.rbImage.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbImage.Size = new System.Drawing.Size(150, 29);
            this.rbImage.TabIndex = 26;
            this.rbImage.Text = "图片水印";
            this.rbImage.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.rbImage.CheckedChanged += new System.EventHandler(this.rbImage_CheckedChanged);
            // 
            // rbFont
            // 
            this.rbFont.Checked = true;
            this.rbFont.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbFont.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rbFont.Location = new System.Drawing.Point(34, 82);
            this.rbFont.MinimumSize = new System.Drawing.Size(1, 1);
            this.rbFont.Name = "rbFont";
            this.rbFont.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbFont.Size = new System.Drawing.Size(150, 29);
            this.rbFont.TabIndex = 25;
            this.rbFont.Text = "文字水印";
            this.rbFont.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.rbFont.CheckedChanged += new System.EventHandler(this.rbFont_CheckedChanged);
            // 
            // btnDoWatermark
            // 
            this.btnDoWatermark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDoWatermark.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDoWatermark.Location = new System.Drawing.Point(417, 277);
            this.btnDoWatermark.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnDoWatermark.Name = "btnDoWatermark";
            this.btnDoWatermark.Size = new System.Drawing.Size(239, 56);
            this.btnDoWatermark.TabIndex = 18;
            this.btnDoWatermark.Text = "加水印";
            this.btnDoWatermark.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDoWatermark.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnDoWatermark.Click += new System.EventHandler(this.btnDoWatermark_Click);
            // 
            // btnWaterViewPdf
            // 
            this.btnWaterViewPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWaterViewPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnWaterViewPdf.Location = new System.Drawing.Point(534, 82);
            this.btnWaterViewPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnWaterViewPdf.Name = "btnWaterViewPdf";
            this.btnWaterViewPdf.Size = new System.Drawing.Size(121, 32);
            this.btnWaterViewPdf.Symbol = 61550;
            this.btnWaterViewPdf.TabIndex = 17;
            this.btnWaterViewPdf.Text = "预览";
            this.btnWaterViewPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnWaterViewPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnWaterViewPdf.Click += new System.EventHandler(this.btnWaterViewPdf_Click);
            // 
            // btnSelectWatermarkPdf
            // 
            this.btnSelectWatermarkPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectWatermarkPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectWatermarkPdf.Location = new System.Drawing.Point(533, 42);
            this.btnSelectWatermarkPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelectWatermarkPdf.Name = "btnSelectWatermarkPdf";
            this.btnSelectWatermarkPdf.Size = new System.Drawing.Size(122, 32);
            this.btnSelectWatermarkPdf.Symbol = 61564;
            this.btnSelectWatermarkPdf.TabIndex = 16;
            this.btnSelectWatermarkPdf.Text = "选择文件";
            this.btnSelectWatermarkPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectWatermarkPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSelectWatermarkPdf.Click += new System.EventHandler(this.btnSelectWatermarkPdf_Click);
            // 
            // tWatermarkPdf
            // 
            this.tWatermarkPdf.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tWatermarkPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tWatermarkPdf.Location = new System.Drawing.Point(129, 42);
            this.tWatermarkPdf.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tWatermarkPdf.MinimumSize = new System.Drawing.Size(1, 16);
            this.tWatermarkPdf.Name = "tWatermarkPdf";
            this.tWatermarkPdf.ReadOnly = true;
            this.tWatermarkPdf.ShowText = false;
            this.tWatermarkPdf.Size = new System.Drawing.Size(397, 32);
            this.tWatermarkPdf.TabIndex = 14;
            this.tWatermarkPdf.TagString = "";
            this.tWatermarkPdf.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tWatermarkPdf.Watermark = "";
            this.tWatermarkPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(25, 42);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(107, 27);
            this.uiLabel1.TabIndex = 12;
            this.uiLabel1.Text = "文件路径:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBoxWatermark
            // 
            this.uiGroupBoxWatermark.Controls.Add(this.uiPanelImage);
            this.uiGroupBoxWatermark.Controls.Add(this.uiPanelFont);
            this.uiGroupBoxWatermark.Controls.Add(this.rbImage);
            this.uiGroupBoxWatermark.Controls.Add(this.rbFont);
            this.uiGroupBoxWatermark.Controls.Add(this.btnDoWatermark);
            this.uiGroupBoxWatermark.Controls.Add(this.btnWaterViewPdf);
            this.uiGroupBoxWatermark.Controls.Add(this.btnSelectWatermarkPdf);
            this.uiGroupBoxWatermark.Controls.Add(this.tWatermarkPdf);
            this.uiGroupBoxWatermark.Controls.Add(this.uiLabel1);
            this.uiGroupBoxWatermark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxWatermark.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxWatermark.Location = new System.Drawing.Point(10, 35);
            this.uiGroupBoxWatermark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxWatermark.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxWatermark.Name = "uiGroupBoxWatermark";
            this.uiGroupBoxWatermark.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBoxWatermark.Size = new System.Drawing.Size(674, 349);
            this.uiGroupBoxWatermark.TabIndex = 1;
            this.uiGroupBoxWatermark.Text = "水印";
            this.uiGroupBoxWatermark.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxWatermark.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiPanelImage
            // 
            this.uiPanelImage.Controls.Add(this.uiDoubleUpDownScale);
            this.uiPanelImage.Controls.Add(this.uiLabel9);
            this.uiPanelImage.Controls.Add(this.uiIntegerUpDownRadAngle);
            this.uiPanelImage.Controls.Add(this.uiIntegerUpDownBottom);
            this.uiPanelImage.Controls.Add(this.uiLabel7);
            this.uiPanelImage.Controls.Add(this.uiLabel8);
            this.uiPanelImage.Controls.Add(this.uiIntegerUpDownLeft);
            this.uiPanelImage.Controls.Add(this.uiLabel6);
            this.uiPanelImage.Controls.Add(this.uiTextBoxImagePath);
            this.uiPanelImage.Controls.Add(this.uiLabel5);
            this.uiPanelImage.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanelImage.Location = new System.Drawing.Point(35, 117);
            this.uiPanelImage.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanelImage.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanelImage.Name = "uiPanelImage";
            this.uiPanelImage.Size = new System.Drawing.Size(621, 148);
            this.uiPanelImage.TabIndex = 30;
            this.uiPanelImage.Text = null;
            this.uiPanelImage.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPanelImage.Visible = false;
            this.uiPanelImage.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiDoubleUpDownScale
            // 
            this.uiDoubleUpDownScale.DecimalPlaces = 2;
            this.uiDoubleUpDownScale.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiDoubleUpDownScale.Location = new System.Drawing.Point(106, 95);
            this.uiDoubleUpDownScale.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiDoubleUpDownScale.Minimum = 0D;
            this.uiDoubleUpDownScale.MinimumSize = new System.Drawing.Size(100, 0);
            this.uiDoubleUpDownScale.Name = "uiDoubleUpDownScale";
            this.uiDoubleUpDownScale.ShowText = false;
            this.uiDoubleUpDownScale.Size = new System.Drawing.Size(152, 29);
            this.uiDoubleUpDownScale.Step = 0.01D;
            this.uiDoubleUpDownScale.TabIndex = 37;
            this.uiDoubleUpDownScale.Text = "uiDoubleUpDown1";
            this.uiDoubleUpDownScale.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiDoubleUpDownScale.Value = 1D;
            this.uiDoubleUpDownScale.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel9
            // 
            this.uiLabel9.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel9.Location = new System.Drawing.Point(14, 96);
            this.uiLabel9.Name = "uiLabel9";
            this.uiLabel9.Size = new System.Drawing.Size(100, 23);
            this.uiLabel9.TabIndex = 37;
            this.uiLabel9.Text = "缩放比例:";
            this.uiLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel9.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiIntegerUpDownRadAngle
            // 
            this.uiIntegerUpDownRadAngle.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiIntegerUpDownRadAngle.Location = new System.Drawing.Point(106, 53);
            this.uiIntegerUpDownRadAngle.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiIntegerUpDownRadAngle.Maximum = 90;
            this.uiIntegerUpDownRadAngle.Minimum = -90;
            this.uiIntegerUpDownRadAngle.MinimumSize = new System.Drawing.Size(100, 0);
            this.uiIntegerUpDownRadAngle.Name = "uiIntegerUpDownRadAngle";
            this.uiIntegerUpDownRadAngle.ShowText = false;
            this.uiIntegerUpDownRadAngle.Size = new System.Drawing.Size(152, 29);
            this.uiIntegerUpDownRadAngle.Step = 5;
            this.uiIntegerUpDownRadAngle.TabIndex = 36;
            this.uiIntegerUpDownRadAngle.Text = null;
            this.uiIntegerUpDownRadAngle.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiIntegerUpDownRadAngle.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiIntegerUpDownBottom
            // 
            this.uiIntegerUpDownBottom.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiIntegerUpDownBottom.Location = new System.Drawing.Point(316, 94);
            this.uiIntegerUpDownBottom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiIntegerUpDownBottom.Maximum = 100000;
            this.uiIntegerUpDownBottom.Minimum = 0;
            this.uiIntegerUpDownBottom.MinimumSize = new System.Drawing.Size(100, 0);
            this.uiIntegerUpDownBottom.Name = "uiIntegerUpDownBottom";
            this.uiIntegerUpDownBottom.ShowText = false;
            this.uiIntegerUpDownBottom.Size = new System.Drawing.Size(152, 29);
            this.uiIntegerUpDownBottom.Step = 5;
            this.uiIntegerUpDownBottom.TabIndex = 34;
            this.uiIntegerUpDownBottom.Text = null;
            this.uiIntegerUpDownBottom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiIntegerUpDownBottom.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel7.Location = new System.Drawing.Point(283, 94);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(41, 23);
            this.uiLabel7.TabIndex = 33;
            this.uiLabel7.Text = "下:";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel7.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel8
            // 
            this.uiLabel8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel8.Location = new System.Drawing.Point(14, 55);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.Size = new System.Drawing.Size(100, 23);
            this.uiLabel8.TabIndex = 35;
            this.uiLabel8.Text = "旋转角度:";
            this.uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel8.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiIntegerUpDownLeft
            // 
            this.uiIntegerUpDownLeft.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiIntegerUpDownLeft.Location = new System.Drawing.Point(316, 53);
            this.uiIntegerUpDownLeft.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiIntegerUpDownLeft.Maximum = 10000;
            this.uiIntegerUpDownLeft.Minimum = 0;
            this.uiIntegerUpDownLeft.MinimumSize = new System.Drawing.Size(100, 0);
            this.uiIntegerUpDownLeft.Name = "uiIntegerUpDownLeft";
            this.uiIntegerUpDownLeft.ShowText = false;
            this.uiIntegerUpDownLeft.Size = new System.Drawing.Size(152, 29);
            this.uiIntegerUpDownLeft.Step = 5;
            this.uiIntegerUpDownLeft.TabIndex = 32;
            this.uiIntegerUpDownLeft.Text = null;
            this.uiIntegerUpDownLeft.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiIntegerUpDownLeft.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel6.Location = new System.Drawing.Point(284, 54);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(41, 23);
            this.uiLabel6.TabIndex = 31;
            this.uiLabel6.Text = "左:";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel6.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiTextBoxImagePath
            // 
            this.uiTextBoxImagePath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBoxImagePath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiTextBoxImagePath.Location = new System.Drawing.Point(106, 15);
            this.uiTextBoxImagePath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBoxImagePath.MinimumSize = new System.Drawing.Size(1, 16);
            this.uiTextBoxImagePath.Name = "uiTextBoxImagePath";
            this.uiTextBoxImagePath.ReadOnly = true;
            this.uiTextBoxImagePath.ShowButton = true;
            this.uiTextBoxImagePath.ShowText = false;
            this.uiTextBoxImagePath.Size = new System.Drawing.Size(362, 32);
            this.uiTextBoxImagePath.TabIndex = 15;
            this.uiTextBoxImagePath.TagString = "";
            this.uiTextBoxImagePath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiTextBoxImagePath.Watermark = "";
            this.uiTextBoxImagePath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiTextBoxImagePath.ButtonClick += new System.EventHandler(this.uiTextBoxImagePath_ButtonClick);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel5.Location = new System.Drawing.Point(15, 18);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(115, 23);
            this.uiLabel5.TabIndex = 0;
            this.uiLabel5.Text = "图片路径:";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel5.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // PdfWatermarkForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(694, 394);
            this.Controls.Add(this.uiGroupBoxWatermark);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PdfWatermarkForm";
            this.Padding = new System.Windows.Forms.Padding(10, 35, 10, 10);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "水印";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiPanelFont.ResumeLayout(false);
            this.uiGroupBoxWatermark.ResumeLayout(false);
            this.uiPanelImage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private Sunny.UI.UIPanel uiPanelFont;
        private Sunny.UI.UIIntegerUpDown numFontSize;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UIIntegerUpDown numAngle;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox tWatermark;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UIRadioButton rbImage;
        private Sunny.UI.UIRadioButton rbFont;
        private Sunny.UI.UIButton btnDoWatermark;
        private Sunny.UI.UISymbolButton btnWaterViewPdf;
        private Sunny.UI.UISymbolButton btnSelectWatermarkPdf;
        private Sunny.UI.UITextBox tWatermarkPdf;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIGroupBox uiGroupBoxWatermark;
        private Sunny.UI.UIPanel uiPanelImage;
        private Sunny.UI.UIDoubleUpDown uiDoubleUpDownScale;
        private Sunny.UI.UILabel uiLabel9;
        private Sunny.UI.UIIntegerUpDown uiIntegerUpDownRadAngle;
        private Sunny.UI.UIIntegerUpDown uiIntegerUpDownBottom;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UIIntegerUpDown uiIntegerUpDownLeft;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UITextBox uiTextBoxImagePath;
        private Sunny.UI.UILabel uiLabel5;
    }
}