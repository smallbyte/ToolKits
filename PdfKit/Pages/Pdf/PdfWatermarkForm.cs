﻿using Base.Platform;
using iText.IO.Font;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Layer;
using iText.Layout;
using iText.Layout.Element;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Pdf
{
    public partial class PdfWatermarkForm : UIForm
    {
        public PdfWatermarkForm(Module module)
        {
            InitializeComponent();
        }


        private void btnSelectWatermarkPdf_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Pdf文件|*.pdf" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tWatermarkPdf.Text = openFileDialog.FileName;
                }
            }
        }

        private void btnWaterViewPdf_Click(object sender, EventArgs e)
        {
            this.showPdfView(tWatermarkPdf.Text);
        }

        private void showPdfView(string path, string title = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            FileKit.Common.Utils.ShowPdf(path);
            //PdfView pdfView = new PdfView(path, title);
            //pdfView.Show();
        }

        private void btnDoWatermark_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tWatermarkPdf.Text))
            {
                ShowWarningDialog("提示", "请选择待加水印文件！");
                //MessageBox.Show("请选择待加水印文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string srcPdf = tWatermarkPdf.Text;
            string targetPdf = string.Format("{0}{1}.pdf", System.IO.Path.GetTempPath(), Guid.NewGuid());
            if (rbFont.Checked)
            {
                if (string.IsNullOrEmpty(tWatermark.Text))
                {
                    ShowWarningDialog("提示", "请输入水印内容！");
                    //MessageBox.Show("请输入水印内容！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (numAngle.Value < -90 || numAngle.Value > 90)
                {
                    ShowWarningDialog("提示", "请输入-90 ~ 90旋转角度！");
                    //MessageBox.Show("请输入-90 ~ 90旋转角度！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (File.Exists(targetPdf))
                    File.Delete(targetPdf);
                string markStr = tWatermark.Text;
                int fontSize = Decimal.ToInt32(numFontSize.Value);
                uiGroupBoxWatermark.Enabled = false;
                Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
                {
                    try
                    {
                        PdfReader reader = new PdfReader(srcPdf);
                        PdfWriter writer = new PdfWriter(targetPdf);
                        PdfDocument pdfDoc = new PdfDocument(reader, writer);

                        PdfFont font = PdfFontFactory.CreateFont(string.Format("{0}simsun.ttf", AppDomain.CurrentDomain.BaseDirectory), PdfEncodings.IDENTITY_H);
                        Paragraph paragraph = new Paragraph(markStr).SetFont(font).SetFontSize(fontSize);
                        paragraph.SetFontColor(ColorConstants.BLACK, 0.2f);

                        int numberOfPages = pdfDoc.GetNumberOfPages();
                        for (int i = 1; i <= numberOfPages; i++)
                        {
                            PdfPage page = pdfDoc.GetPage(i);
                            iText.Kernel.Geom.Rectangle ps = page.GetPageSize();

                            float pageHeight = ps.GetHeight();
                            float pageWidth = ps.GetWidth();

                            PdfLayer layer = new PdfLayer("watermark", pdfDoc);
                            PdfCanvas canvas = new PdfCanvas(page);
                            canvas.BeginLayer(layer);

                            Canvas canvasModel = new Canvas(canvas, ps);

                            int angle = Decimal.ToInt32(numAngle.Value);
                            float radAngle = float.Parse(((Math.PI / 180) * angle).ToString());
                            canvasModel.ShowTextAligned(paragraph, pageWidth / 2, pageHeight / 2, i, iText.Layout.Properties.TextAlignment.CENTER, iText.Layout.Properties.VerticalAlignment.MIDDLE, radAngle);
                            canvasModel.SetFontColor(ColorConstants.BLACK, 0.2f);
                            canvas.EndLayer();
                        }

                        pdfDoc.Close();
                    }
                    catch (Exception ex)
                    {
                        ExceptionManager.TraceWrite(ex);
                    }
                }, (s2, e2) =>
                {
                    uiGroupBoxWatermark.Enabled = true;
                    this.showPdfView(targetPdf, "预览加水印文件（请点击下列的图标进行操作！）");
                });
            }
            else
            {
                if (string.IsNullOrEmpty(uiTextBoxImagePath.Text))
                {
                    ShowWarningDialog("提示", "请选取水印图片！");
                    //MessageBox.Show("请输入水印内容！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (File.Exists(targetPdf))
                    File.Delete(targetPdf);
                uiGroupBoxWatermark.Enabled = false;
                Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
                {
                    try
                    {
                        PdfReader reader = new PdfReader(srcPdf);
                        PdfWriter writer = new PdfWriter(targetPdf);
                        PdfDocument pdfDoc = new PdfDocument(reader, writer);

                        int numberOfPages = pdfDoc.GetNumberOfPages();
                        for (int i = 1; i <= numberOfPages; i++)
                        {
                            PdfPage page = pdfDoc.GetPage(i);
                            iText.Kernel.Geom.Rectangle ps = page.GetPageSize();

                            float pageHeight = ps.GetHeight();
                            float pageWidth = ps.GetWidth();

                            PdfLayer layer = new PdfLayer("watermark", pdfDoc);
                            PdfCanvas canvas = new PdfCanvas(page);
                            canvas.BeginLayer(layer);

                            Canvas canvasModel = new Canvas(canvas, ps);

                            ImageData imageData = ImageDataFactory.Create(uiTextBoxImagePath.Text, false);
                            iText.Layout.Element.Image image = new iText.Layout.Element.Image(imageData);

                            float width = imageData.GetWidth();
                            float height = imageData.GetHeight();

                            image.SetWidth(width * float.Parse(uiDoubleUpDownScale.Value.ToString()));
                            image.SetHeight(height * float.Parse(uiDoubleUpDownScale.Value.ToString()));
                            image.SetFixedPosition(uiIntegerUpDownLeft.Value, uiIntegerUpDownBottom.Value);


                            int angle = Decimal.ToInt32(uiIntegerUpDownRadAngle.Value);
                            float radAngle = float.Parse(((Math.PI / 180) * angle).ToString());
                            image.SetRotationAngle(angle);
                            canvasModel.Add(image);

                            canvas.EndLayer();
                        }

                        pdfDoc.Close();
                    }
                    catch (Exception ex)
                    {
                        ExceptionManager.TraceWrite(ex);
                    }
                }, (s2, e2) =>
                {
                    uiGroupBoxWatermark.Enabled = true;
                    this.showPdfView(targetPdf, "预览加水印文件（请点击下列的图标进行操作！）");
                });
            }
        }

        private void rbFont_CheckedChanged(object sender, EventArgs e)
        {
            uiPanelFont.Visible = rbFont.Checked;
        }

        private void rbImage_CheckedChanged(object sender, EventArgs e)
        {
            uiPanelImage.Visible = rbImage.Checked;
        }

        private void uiTextBoxImagePath_ButtonClick(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Png图片文件|*.png" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    uiTextBoxImagePath.Text = openFileDialog.FileName;
                }
            }
        }
    }
}
