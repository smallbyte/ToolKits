﻿namespace FileKit.Pages.Pdf
{
    partial class PdfToImageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PdfToImageForm));
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.btnOpenDir = new Sunny.UI.UISymbolButton();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.lbMain = new Sunny.UI.UIListBox();
            this.lbMsg = new Sunny.UI.UILabel();
            this.btnConvert = new Sunny.UI.UIButton();
            this.btnViewPdf = new Sunny.UI.UISymbolButton();
            this.btnSelectPdf = new Sunny.UI.UISymbolButton();
            this.tPdfPath = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.uiToolTipMain = new Sunny.UI.UIToolTip(this.components);
            this.uiGroupBoxMain.SuspendLayout();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.btnOpenDir);
            this.uiGroupBoxMain.Controls.Add(this.uiGroupBox1);
            this.uiGroupBoxMain.Controls.Add(this.lbMsg);
            this.uiGroupBoxMain.Controls.Add(this.btnConvert);
            this.uiGroupBoxMain.Controls.Add(this.btnViewPdf);
            this.uiGroupBoxMain.Controls.Add(this.btnSelectPdf);
            this.uiGroupBoxMain.Controls.Add(this.tPdfPath);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel1);
            this.uiGroupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMain.Location = new System.Drawing.Point(10, 35);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(2, 32, 2, 2);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(780, 401);
            this.uiGroupBoxMain.TabIndex = 1;
            this.uiGroupBoxMain.Text = "Pdf转图片";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnOpenDir
            // 
            this.btnOpenDir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenDir.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpenDir.Location = new System.Drawing.Point(533, 333);
            this.btnOpenDir.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnOpenDir.Name = "btnOpenDir";
            this.btnOpenDir.Size = new System.Drawing.Size(153, 32);
            this.btnOpenDir.Symbol = 61564;
            this.btnOpenDir.TabIndex = 16;
            this.btnOpenDir.Text = "打开文件夹";
            this.btnOpenDir.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpenDir.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnOpenDir.Click += new System.EventHandler(this.btnOpenDir_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.lbMain);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBox1.Location = new System.Drawing.Point(26, 80);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBox1.Size = new System.Drawing.Size(492, 285);
            this.uiGroupBox1.TabIndex = 15;
            this.uiGroupBox1.Text = "图片列表";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // lbMain
            // 
            this.lbMain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMain.FillColor = System.Drawing.Color.White;
            this.lbMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbMain.Location = new System.Drawing.Point(5, 32);
            this.lbMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.lbMain.Name = "lbMain";
            this.lbMain.Padding = new System.Windows.Forms.Padding(2);
            this.lbMain.ShowText = false;
            this.lbMain.Size = new System.Drawing.Size(482, 248);
            this.lbMain.TabIndex = 0;
            this.lbMain.Text = null;
            this.lbMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.lbMain.DoubleClick += new System.EventHandler(this.lbMain_ItemDoubleClick);
            // 
            // lbMsg
            // 
            this.lbMsg.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.lbMsg.ForeColor = System.Drawing.Color.Red;
            this.lbMsg.Location = new System.Drawing.Point(27, 370);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(275, 23);
            this.lbMsg.Style = Sunny.UI.UIStyle.Custom;
            this.lbMsg.StyleCustomMode = true;
            this.lbMsg.TabIndex = 14;
            this.lbMsg.Text = "...";
            this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMsg.Visible = false;
            this.lbMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnConvert
            // 
            this.btnConvert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConvert.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnConvert.Location = new System.Drawing.Point(533, 76);
            this.btnConvert.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(242, 59);
            this.btnConvert.TabIndex = 13;
            this.btnConvert.Text = "转化";
            this.btnConvert.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnConvert.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // btnViewPdf
            // 
            this.btnViewPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewPdf.Location = new System.Drawing.Point(662, 38);
            this.btnViewPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnViewPdf.Name = "btnViewPdf";
            this.btnViewPdf.Size = new System.Drawing.Size(113, 32);
            this.btnViewPdf.Symbol = 61550;
            this.btnViewPdf.TabIndex = 9;
            this.btnViewPdf.Text = "预览";
            this.btnViewPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnViewPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnViewPdf.Click += new System.EventHandler(this.btnViewPdf_Click);
            // 
            // btnSelectPdf
            // 
            this.btnSelectPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPdf.Location = new System.Drawing.Point(533, 38);
            this.btnSelectPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelectPdf.Name = "btnSelectPdf";
            this.btnSelectPdf.Size = new System.Drawing.Size(122, 32);
            this.btnSelectPdf.Symbol = 61564;
            this.btnSelectPdf.TabIndex = 8;
            this.btnSelectPdf.Text = "选择文件";
            this.btnSelectPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSelectPdf.Click += new System.EventHandler(this.btnSelectPdf_Click);
            // 
            // tPdfPath
            // 
            this.tPdfPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPdfPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tPdfPath.Location = new System.Drawing.Point(121, 38);
            this.tPdfPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPdfPath.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPdfPath.Name = "tPdfPath";
            this.tPdfPath.ReadOnly = true;
            this.tPdfPath.ShowText = false;
            this.tPdfPath.Size = new System.Drawing.Size(397, 32);
            this.tPdfPath.TabIndex = 7;
            this.tPdfPath.TagString = "";
            this.tPdfPath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPdfPath.Watermark = "";
            this.tPdfPath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(21, 38);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(107, 27);
            this.uiLabel1.TabIndex = 6;
            this.uiLabel1.Text = "文件路径:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiToolTipMain
            // 
            this.uiToolTipMain.BackColor = System.Drawing.Color.White;
            this.uiToolTipMain.ForeColor = System.Drawing.Color.Black;
            this.uiToolTipMain.OwnerDraw = true;
            // 
            // PdfToImageForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 446);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PdfToImageForm";
            this.Padding = new System.Windows.Forms.Padding(10, 35, 10, 10);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "转图片";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBoxMain;
        private Sunny.UI.UISymbolButton btnOpenDir;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UIListBox lbMain;
        private Sunny.UI.UILabel lbMsg;
        private Sunny.UI.UIButton btnConvert;
        private Sunny.UI.UISymbolButton btnViewPdf;
        private Sunny.UI.UISymbolButton btnSelectPdf;
        private Sunny.UI.UITextBox tPdfPath;
        private Sunny.UI.UILabel uiLabel1;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private Sunny.UI.UIToolTip uiToolTipMain;
    }
}