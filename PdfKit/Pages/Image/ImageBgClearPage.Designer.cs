﻿namespace FileKit.Pages.Image
{
    partial class ImageBgClearPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageBgClearPage));
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.uiGroupBox2 = new Sunny.UI.UIGroupBox();
            this.pictureBoxResult = new System.Windows.Forms.PictureBox();
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.pictureBoxMain = new System.Windows.Forms.PictureBox();
            this.uiButtonClear = new Sunny.UI.UIButton();
            this.btnSelectPdf = new Sunny.UI.UISymbolButton();
            this.tPdfPath = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiGroupBoxMain.SuspendLayout();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxResult)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.uiGroupBox2);
            this.uiGroupBoxMain.Controls.Add(this.uiGroupBox1);
            this.uiGroupBoxMain.Controls.Add(this.uiButtonClear);
            this.uiGroupBoxMain.Controls.Add(this.btnSelectPdf);
            this.uiGroupBoxMain.Controls.Add(this.tPdfPath);
            this.uiGroupBoxMain.Controls.Add(this.uiLabel1);
            this.uiGroupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMain.Location = new System.Drawing.Point(10, 35);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(2, 32, 2, 2);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(697, 435);
            this.uiGroupBoxMain.TabIndex = 1;
            this.uiGroupBoxMain.Text = "背景清除";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.pictureBoxResult);
            this.uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBox2.Location = new System.Drawing.Point(293, 88);
            this.uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBox2.Size = new System.Drawing.Size(241, 330);
            this.uiGroupBox2.TabIndex = 23;
            this.uiGroupBox2.Text = "结果";
            this.uiGroupBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // pictureBoxResult
            // 
            this.pictureBoxResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxResult.Location = new System.Drawing.Point(5, 32);
            this.pictureBoxResult.Name = "pictureBoxResult";
            this.pictureBoxResult.Size = new System.Drawing.Size(231, 293);
            this.pictureBoxResult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxResult.TabIndex = 21;
            this.pictureBoxResult.TabStop = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.pictureBoxMain);
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBox1.Location = new System.Drawing.Point(24, 88);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBox1.Size = new System.Drawing.Size(261, 330);
            this.uiGroupBox1.TabIndex = 22;
            this.uiGroupBox1.Text = "原图";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // pictureBoxMain
            // 
            this.pictureBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxMain.Location = new System.Drawing.Point(5, 32);
            this.pictureBoxMain.Name = "pictureBoxMain";
            this.pictureBoxMain.Size = new System.Drawing.Size(251, 293);
            this.pictureBoxMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxMain.TabIndex = 21;
            this.pictureBoxMain.TabStop = false;
            // 
            // uiButtonClear
            // 
            this.uiButtonClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButtonClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButtonClear.Location = new System.Drawing.Point(541, 88);
            this.uiButtonClear.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButtonClear.Name = "uiButtonClear";
            this.uiButtonClear.Size = new System.Drawing.Size(121, 32);
            this.uiButtonClear.TabIndex = 20;
            this.uiButtonClear.Text = "去除背景";
            this.uiButtonClear.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButtonClear.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiButtonClear.Click += new System.EventHandler(this.uiButtonClear_Click);
            // 
            // btnSelectPdf
            // 
            this.btnSelectPdf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectPdf.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPdf.Location = new System.Drawing.Point(541, 46);
            this.btnSelectPdf.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelectPdf.Name = "btnSelectPdf";
            this.btnSelectPdf.Size = new System.Drawing.Size(122, 32);
            this.btnSelectPdf.Symbol = 61564;
            this.btnSelectPdf.TabIndex = 19;
            this.btnSelectPdf.Text = "选择文件";
            this.btnSelectPdf.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPdf.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSelectPdf.Click += new System.EventHandler(this.btnSelectPdf_Click);
            // 
            // tPdfPath
            // 
            this.tPdfPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPdfPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tPdfPath.Location = new System.Drawing.Point(117, 46);
            this.tPdfPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPdfPath.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPdfPath.Name = "tPdfPath";
            this.tPdfPath.ReadOnly = true;
            this.tPdfPath.ShowText = false;
            this.tPdfPath.Size = new System.Drawing.Size(417, 32);
            this.tPdfPath.TabIndex = 18;
            this.tPdfPath.TagString = "";
            this.tPdfPath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPdfPath.Watermark = "";
            this.tPdfPath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(19, 46);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(107, 27);
            this.uiLabel1.TabIndex = 17;
            this.uiLabel1.Text = "文件路径:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // ImageBgClearPage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(717, 480);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ImageBgClearPage";
            this.Padding = new System.Windows.Forms.Padding(10, 35, 10, 10);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "背景清除";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxResult)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIGroupBox uiGroupBoxMain;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.PictureBox pictureBoxResult;
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.PictureBox pictureBoxMain;
        private Sunny.UI.UIButton uiButtonClear;
        private Sunny.UI.UISymbolButton btnSelectPdf;
        private Sunny.UI.UITextBox tPdfPath;
        private Sunny.UI.UILabel uiLabel1;
    }
}