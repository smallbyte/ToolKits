﻿namespace FileKit.Pages.Image
{
    partial class ImageToIconPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageToIconPage));
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.uiListBoxMain = new Sunny.UI.UIListBox();
            this.uiSymbolButtonSelect = new Sunny.UI.UISymbolButton();
            this.uiCheckBoxGroupMain = new Sunny.UI.UICheckBoxGroup();
            this.uiSymbolButtonClear = new Sunny.UI.UISymbolButton();
            this.uiSymbolButtonOK = new Sunny.UI.UISymbolButton();
            this.uiLabelMsg = new Sunny.UI.UILabel();
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.uiSymbolButtonFolder = new Sunny.UI.UISymbolButton();
            this.uiSymbolButtonRemove = new Sunny.UI.UISymbolButton();
            this.uiGroupBoxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel1.Location = new System.Drawing.Point(336, 57);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(400, 40);
            this.uiLabel1.TabIndex = 3;
            this.uiLabel1.Text = "请选择需要转换的图片，支持png、jpg、bmp！";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.uiListBoxMain);
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMain.Location = new System.Drawing.Point(22, 107);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(302, 300);
            this.uiGroupBoxMain.TabIndex = 4;
            this.uiGroupBoxMain.Text = "已选图片或者路径";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiListBoxMain
            // 
            this.uiListBoxMain.AllowDrop = true;
            this.uiListBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBoxMain.FillColor = System.Drawing.Color.White;
            this.uiListBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiListBoxMain.Location = new System.Drawing.Point(5, 32);
            this.uiListBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiListBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiListBoxMain.Name = "uiListBoxMain";
            this.uiListBoxMain.Padding = new System.Windows.Forms.Padding(2);
            this.uiListBoxMain.ShowText = false;
            this.uiListBoxMain.Size = new System.Drawing.Size(292, 263);
            this.uiListBoxMain.TabIndex = 0;
            this.uiListBoxMain.Text = "uiListBoxMain";
            this.uiListBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiListBoxMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.uiListBoxMain_DragDrop);
            this.uiListBoxMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.uiListBoxMain_DragEnter);
            // 
            // uiSymbolButtonSelect
            // 
            this.uiSymbolButtonSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiSymbolButtonSelect.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonSelect.Location = new System.Drawing.Point(27, 56);
            this.uiSymbolButtonSelect.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiSymbolButtonSelect.Name = "uiSymbolButtonSelect";
            this.uiSymbolButtonSelect.Size = new System.Drawing.Size(134, 41);
            this.uiSymbolButtonSelect.Symbol = 61502;
            this.uiSymbolButtonSelect.TabIndex = 5;
            this.uiSymbolButtonSelect.Text = "选择图片";
            this.uiSymbolButtonSelect.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonSelect.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSymbolButtonSelect.Click += new System.EventHandler(this.uiSymbolButtonSelect_Click);
            // 
            // uiCheckBoxGroupMain
            // 
            this.uiCheckBoxGroupMain.ColumnCount = 3;
            this.uiCheckBoxGroupMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiCheckBoxGroupMain.Location = new System.Drawing.Point(349, 107);
            this.uiCheckBoxGroupMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiCheckBoxGroupMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiCheckBoxGroupMain.Name = "uiCheckBoxGroupMain";
            this.uiCheckBoxGroupMain.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiCheckBoxGroupMain.SelectedIndexes = ((System.Collections.Generic.List<int>)(resources.GetObject("uiCheckBoxGroupMain.SelectedIndexes")));
            this.uiCheckBoxGroupMain.Size = new System.Drawing.Size(442, 142);
            this.uiCheckBoxGroupMain.TabIndex = 6;
            this.uiCheckBoxGroupMain.Text = "大小";
            this.uiCheckBoxGroupMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiCheckBoxGroupMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiSymbolButtonClear
            // 
            this.uiSymbolButtonClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiSymbolButtonClear.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonClear.Location = new System.Drawing.Point(229, 411);
            this.uiSymbolButtonClear.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiSymbolButtonClear.Name = "uiSymbolButtonClear";
            this.uiSymbolButtonClear.Size = new System.Drawing.Size(95, 31);
            this.uiSymbolButtonClear.Symbol = 57369;
            this.uiSymbolButtonClear.SymbolSize = 20;
            this.uiSymbolButtonClear.TabIndex = 7;
            this.uiSymbolButtonClear.Text = "清空";
            this.uiSymbolButtonClear.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonClear.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSymbolButtonClear.Click += new System.EventHandler(this.uiSymbolButtonClear_Click);
            // 
            // uiSymbolButtonOK
            // 
            this.uiSymbolButtonOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiSymbolButtonOK.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonOK.Location = new System.Drawing.Point(537, 257);
            this.uiSymbolButtonOK.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiSymbolButtonOK.Name = "uiSymbolButtonOK";
            this.uiSymbolButtonOK.Size = new System.Drawing.Size(254, 51);
            this.uiSymbolButtonOK.TabIndex = 8;
            this.uiSymbolButtonOK.Text = "开始转换";
            this.uiSymbolButtonOK.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonOK.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSymbolButtonOK.Click += new System.EventHandler(this.uiSymbolButtonOK_Click);
            // 
            // uiLabelMsg
            // 
            this.uiLabelMsg.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabelMsg.Location = new System.Drawing.Point(349, 378);
            this.uiLabelMsg.Name = "uiLabelMsg";
            this.uiLabelMsg.Size = new System.Drawing.Size(100, 23);
            this.uiLabelMsg.TabIndex = 9;
            this.uiLabelMsg.Text = "...";
            this.uiLabelMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabelMsg.Visible = false;
            this.uiLabelMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiSymbolButtonFolder
            // 
            this.uiSymbolButtonFolder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiSymbolButtonFolder.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonFolder.Location = new System.Drawing.Point(167, 56);
            this.uiSymbolButtonFolder.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiSymbolButtonFolder.Name = "uiSymbolButtonFolder";
            this.uiSymbolButtonFolder.Size = new System.Drawing.Size(152, 41);
            this.uiSymbolButtonFolder.Symbol = 61716;
            this.uiSymbolButtonFolder.TabIndex = 10;
            this.uiSymbolButtonFolder.Text = "选择文件夹";
            this.uiSymbolButtonFolder.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonFolder.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSymbolButtonFolder.Click += new System.EventHandler(this.uiSymbolButtonFolder_Click);
            // 
            // uiSymbolButtonRemove
            // 
            this.uiSymbolButtonRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiSymbolButtonRemove.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonRemove.Location = new System.Drawing.Point(127, 410);
            this.uiSymbolButtonRemove.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiSymbolButtonRemove.Name = "uiSymbolButtonRemove";
            this.uiSymbolButtonRemove.Size = new System.Drawing.Size(96, 31);
            this.uiSymbolButtonRemove.Symbol = 75;
            this.uiSymbolButtonRemove.SymbolSize = 20;
            this.uiSymbolButtonRemove.TabIndex = 11;
            this.uiSymbolButtonRemove.Text = "移除";
            this.uiSymbolButtonRemove.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSymbolButtonRemove.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSymbolButtonRemove.Click += new System.EventHandler(this.uiSymbolButtonRemove_Click);
            // 
            // ImageToIconPage
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.uiSymbolButtonRemove);
            this.Controls.Add(this.uiSymbolButtonFolder);
            this.Controls.Add(this.uiLabelMsg);
            this.Controls.Add(this.uiSymbolButtonOK);
            this.Controls.Add(this.uiSymbolButtonClear);
            this.Controls.Add(this.uiCheckBoxGroupMain);
            this.Controls.Add(this.uiSymbolButtonSelect);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Controls.Add(this.uiLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ImageToIconPage";
            this.Padding = new System.Windows.Forms.Padding(5, 40, 5, 5);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "图片转Icon";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.Load += new System.EventHandler(this.ImageToIconPage_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.uiListBoxMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.uiListBoxMain_DragEnter);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIGroupBox uiGroupBoxMain;
        private Sunny.UI.UIListBox uiListBoxMain;
        private Sunny.UI.UISymbolButton uiSymbolButtonSelect;
        private Sunny.UI.UICheckBoxGroup uiCheckBoxGroupMain;
        private Sunny.UI.UISymbolButton uiSymbolButtonClear;
        private Sunny.UI.UISymbolButton uiSymbolButtonOK;
        private Sunny.UI.UILabel uiLabelMsg;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private Sunny.UI.UISymbolButton uiSymbolButtonFolder;
        private Sunny.UI.UISymbolButton uiSymbolButtonRemove;
    }
}