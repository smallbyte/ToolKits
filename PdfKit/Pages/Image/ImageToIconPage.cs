﻿using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Image
{
    public partial class ImageToIconPage : UIForm
    {
        public ImageToIconPage(Module module)
        {
            InitializeComponent();
        }

        private void ImageToIconPage_Load(object sender, EventArgs e)
        {
            List<ItemSize> sizes = new List<ItemSize>();
            sizes.Add(new ItemSize(16));
            sizes.Add(new ItemSize(24));
            sizes.Add(new ItemSize(36));
            sizes.Add(new ItemSize(48));
            sizes.Add(new ItemSize(64));
            sizes.Add(new ItemSize(72));
            sizes.Add(new ItemSize(128));
            sizes.Add(new ItemSize(256));
            this.uiCheckBoxGroupMain.Items.AddRange(sizes.ToArray());
        }

        private void uiSymbolButtonSelect_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "png文件|*.png|jpg文件|*.jpg|bmp文件|*.bmp" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    this.uiListBoxMain.Items.Add(new PathItemSelected(1, openFileDialog.FileName));
                }
            }
        }

        private void uiSymbolButtonFolder_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog() { RootFolder = Environment.SpecialFolder.MyComputer, Description = "请选择图片所在的文件夹" })
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.uiListBoxMain.Items.Add(new PathItemSelected(0, folderBrowserDialog.SelectedPath));
                }
            }
        }

        private void uiSymbolButtonClear_Click(object sender, EventArgs e)
        {
            this.uiListBoxMain.Items.Clear();
        }

        private void uiSymbolButtonOK_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerMain.IsBusy)
            {
                ShowWarningDialog("警告", "正忙，请稍候...");
                //MessageBox.Show("正忙，请稍候...", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (uiListBoxMain.Items.Count < 1)
            {
                ShowWarningDialog("提示", "请添加至少一个文件或者路径！");
                //MessageBox.Show("请添加至少1个文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            List<Size> sizes = new List<Size>();
            if (uiCheckBoxGroupMain.SelectedItems.Count != 0)
            {
                foreach (object item in uiCheckBoxGroupMain.SelectedItems)
                {
                    int size = ((ItemSize)item).Size;
                    sizes.Add(new Size(size, size));
                }
            }
            else
            {
                ShowWarningDialog("提示", "请选择待转化的图标大小...");
                //MessageBox.Show("正忙，请稍候...", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            List<string> paths = new List<string>();
            foreach (object item in uiListBoxMain.Items)
            {
                PathItemSelected pathItem = item as PathItemSelected;
                if (pathItem.Type == 1)
                    paths.Add(pathItem.Path);
                else
                {
                    var files = Directory.GetFiles(pathItem.Path, "*.*", SearchOption.AllDirectories).Where(s => s.ToLower().EndsWith(".png") || s.ToLower().EndsWith(".jpg") || s.ToLower().EndsWith(".bmp"));
                    foreach (string fileName in files)
                    {
                        paths.Add(fileName);
                    }
                }
            }
            if (paths.Count < 1)
            {
                ShowWarningDialog("提示", "所选路径不包含图片文件！");
                //MessageBox.Show("请添加至少1个文件！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            uiLabelMsg.Visible = true;
            doRunning(true, "开始转化...");
            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                foreach (string path in paths)
                {
                    Base.Common.Utils.Invoke(uiLabelMsg, () =>
                    {
                        uiLabelMsg.Text = string.Format("正在处理 {0}...", System.IO.Path.GetFileName(path));
                    });
                    this.saveToIcon(path, sizes);
                }
            }, (s2, e2) =>
            {
                doRunning(false, "转化完成!");
                ShowSuccessDialog("提示", "处理完成!");
            });
        }

        private void saveToIcon(string imgPath, List<Size> sizes)
        {
            try
            {
                string path = Path.GetDirectoryName(imgPath);
                string fileName = Path.GetFileNameWithoutExtension(imgPath);
                using (Bitmap bm = new Bitmap(imgPath))
                {
                    foreach (Size size in sizes)
                    {
                        string icoPath = Path.Combine(path, fileName + "_" + size.Width.ToString() + "×" + size.Height.ToString() + ".ico");
                        using (Bitmap iconBm = new Bitmap(bm, size))
                        {
                            using (MemoryStream bitMapStream = new MemoryStream())
                            {
                                using (MemoryStream iconStream = new MemoryStream())//存图标的内存流
                                {
                                    iconBm.Save(bitMapStream, ImageFormat.Png); //将原图读取为png格式并存入原图内存流
                                    using (BinaryWriter iconWriter = new BinaryWriter(iconStream)) //新建二进制写入器以写入目标图标内存流
                                    {
                                        iconWriter.Write((short)0);
                                        iconWriter.Write((short)1);
                                        iconWriter.Write((short)1);
                                        iconWriter.Write((byte)iconBm.Width);
                                        iconWriter.Write((byte)iconBm.Height);
                                        iconWriter.Write((short)0);
                                        iconWriter.Write((short)0);
                                        iconWriter.Write((short)32);
                                        iconWriter.Write((int)bitMapStream.Length);
                                        iconWriter.Write(22);
                                        //写入图像体至目标图标内存流
                                        iconWriter.Write(bitMapStream.ToArray());
                                        //保存流，并将流指针定位至头部以Icon对象进行读取输出为文件
                                        iconWriter.Flush();
                                        iconWriter.Seek(0, SeekOrigin.Begin);

                                        Icon icon = new Icon(iconStream);
                                        using (Stream stream = new System.IO.FileStream(icoPath, System.IO.FileMode.Create))
                                        {
                                            icon.Save(stream);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.TraceWrite(ex);
            }
        }

        private void doRunning(bool running, string msg)
        {
            this.uiSymbolButtonClear.Enabled = !running;
            this.uiSymbolButtonOK.Enabled = !running;
            this.uiSymbolButtonSelect.Enabled = !running;
            this.uiSymbolButtonFolder.Enabled = !running;
            this.uiListBoxMain.Enabled = !running;

            uiLabelMsg.Text = msg;
        }

        private void uiListBoxMain_DragDrop(object sender, DragEventArgs e)
        {
            string fileName = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            if (fileName.ToLower().EndsWith(".png") || fileName.ToLower().EndsWith(".jpg") || fileName.ToLower().EndsWith(".bmp"))
                uiListBoxMain.Items.Add(new PathItemSelected(1, fileName));
            else
                ShowWarningDialog("提示", "文件类型不正确，请重新选择！");
        }

        private void uiListBoxMain_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Link;
            else e.Effect = DragDropEffects.None;
        }

        private void uiSymbolButtonRemove_Click(object sender, EventArgs e)
        {
            int index = uiListBoxMain.SelectedIndex;
            if (index == -1)
            {
                ShowWarningDialog("提示", "请选择一项！");
                //MessageBox.Show("请选择一项！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            uiListBoxMain.Items.RemoveAt(uiListBoxMain.SelectedIndex);
        }
    }

    public class PathItemSelected
    {
        public PathItemSelected(int type, string path)
        {
            this.Type = type;
            this.Path = path;
        }
        // 0-文件夹/1-文件
        public int Type;
        public string Path;

        public override string ToString()
        {
            return string.Format("{0}", this.Path);
        }
    }

    public class ItemSize
    {
        public ItemSize(int size)
        {
            this.Size = size;
        }
        public int Size;
        public override string ToString()
        {
            return string.Format("{0}×{0}", this.Size);
        }
    }
}
