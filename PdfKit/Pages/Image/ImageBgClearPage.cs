﻿using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileKit.Pages.Image
{
    public partial class ImageBgClearPage : UIForm
    {
        public ImageBgClearPage(Module module)
        {
            InitializeComponent();
        }

        private void btnSelectPdf_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Png文件|*.png" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tPdfPath.Text = openFileDialog.FileName;
                    pictureBoxMain.Image = System.Drawing.Image.FromFile(tPdfPath.Text);
                    pictureBoxResult.Image = null;
                }
            }
        }

        private void uiButtonClear_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tPdfPath.Text))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            try
            {
                System.Drawing.Image image = System.Drawing.Image.FromFile(tPdfPath.Text);
                Bitmap pbitmap = new Bitmap(image);
                pbitmap.MakeTransparent(Color.White);

                string imgPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(tPdfPath.Text), System.IO.Path.GetFileNameWithoutExtension(tPdfPath.Text) + "_bgclear.png");
                pbitmap.Save(imgPath);

                pictureBoxResult.Image = pbitmap;
                ShowSuccessDialog("提示", "转化完成！");
            }
            catch (Exception ex)
            {
                ShowErrorDialog("异常", ex.Message);
            }

        }
    }
}
