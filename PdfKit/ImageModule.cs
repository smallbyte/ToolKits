﻿using Base.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileKit
{
    public class ImageModule : ModuleList
    {
        public override int GetSymbol()
        {
            return 61502;
        }

        public override string GetName()
        {
            return "图片工具";
        }

        public override Module[] GetModules()
        {
            return new Module[] {
                new Module { ModuleId = 0x00030001u, ModuleName = "背景清除", FormType = typeof(Pages.Image.ImageBgClearPage), ModuleImage = Resource.Clear },
                new Module { ModuleId = 0x00030002u, ModuleName = "图片转Icon", FormType = typeof(Pages.Image.ImageToIconPage), ModuleImage = Resource.ico }
            };
        }

        public override bool Init()
        {
            return true;
        }
    }
}
