﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace FileKit.Common
{
    public partial class Utils
    {
        public static void ShowPdf(string pdfPath)
        {
            try
            {
                string path = Path.Combine(System.Environment.CurrentDirectory, "PdfReader.exe");
                if (!File.Exists(path))
                {
                    System.Diagnostics.Process.Start(pdfPath);
                    return;
                }
                //System.Diagnostics.Process exep = new System.Diagnostics.Process();
                //System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.FileName = path;
                //startInfo.Arguments = pdfPath;
                ////startInfo.CreateNoWindow = true;
                //startInfo.UseShellExecute = false;
                //exep.Start();
                //exep.WaitForExit();

                Process process = new Process();//创建进程对象    
                ProcessStartInfo startInfo = new ProcessStartInfo(path, pdfPath); // 括号里是(程序名,参数)
                process.StartInfo = startInfo;
                process.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void ShowImage(string imgPath)
        {
            //查看原图
            try
            {
                //建立新的系统进程     
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                //设置图片的真实路径和文件名     
                process.StartInfo.FileName = imgPath;

                //设置进程运行参数，这里以最大化窗口方法显示图片。     
                process.StartInfo.Arguments = "rundl132.exe C://WINDOWS//system32//shimgvw.dll,ImageView_Fullscreen";

                //此项为是否使用Shell执行程序，因系统默认为true，此项也可不设，但若设置必须为true     
                process.StartInfo.UseShellExecute = true;

                //此处可以更改进程所打开窗体的显示样式，可以不设     
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.Start();
                process.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
