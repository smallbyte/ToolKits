﻿using Base.Platform;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileKit
{
    public class PdfModule : ModuleList
    {
        public override int GetSymbol()
        {
            return 61889;
        }
        public override string GetName()
        {
            return "Pdf工具";
        }

        public override Module[] GetModules()
        {
            return new Module[] {
                new Module { ModuleId = 0x00020001u, ModuleName = "加密/解密", FormType = typeof(Pages.Pdf.PdfProtectForm), ModuleImage = Resource.Protect },
                new Module { ModuleId = 0x00020002u, ModuleName = "分拆/合并", FormType = typeof(Pages.Pdf.PdfSplitForm), ModuleImage = Resource.Merge },
                new Module { ModuleId = 0x00020003u, ModuleName = "水印", FormType = typeof(Pages.Pdf.PdfWatermarkForm), ModuleImage = Resource.Watermark },
                new Module { ModuleId = 0x00020004u, ModuleName = "转图片", FormType = typeof(Pages.Pdf.PdfToImageForm), ModuleImage = Resource.ToImage }
            };
        }

        public override bool Init()
        {
            return true;
        }
    }
}
