﻿namespace DevKit.Pages
{
    partial class EmailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailForm));
            this.uiGroupBox1 = new Sunny.UI.UIGroupBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.uiComboBoxProtocol = new Sunny.UI.UIComboBox();
            this.tServiceAddress = new Sunny.UI.UITextBox();
            this.tPort = new Sunny.UI.UITextBox();
            this.uiLabel4 = new Sunny.UI.UILabel();
            this.tUserName = new Sunny.UI.UITextBox();
            this.uiLabel3 = new Sunny.UI.UILabel();
            this.tPassword = new Sunny.UI.UITextBox();
            this.uiLabel5 = new Sunny.UI.UILabel();
            this.uiGroupBox2 = new Sunny.UI.UIGroupBox();
            this.tReceivers = new Sunny.UI.UITextBox();
            this.uiLabel6 = new Sunny.UI.UILabel();
            this.tCCAddress = new Sunny.UI.UITextBox();
            this.uiLabel7 = new Sunny.UI.UILabel();
            this.btnSendEmail = new Sunny.UI.UISymbolButton();
            this.uiGroupBox3 = new Sunny.UI.UIGroupBox();
            this.uiGroupBox4 = new Sunny.UI.UIGroupBox();
            this.tSubject = new Sunny.UI.UITextBox();
            this.uiLabel8 = new Sunny.UI.UILabel();
            this.cbIsBodyHtml = new Sunny.UI.UICheckBox();
            this.uiLabel9 = new Sunny.UI.UILabel();
            this.btnAddAttach = new Sunny.UI.UISymbolButton();
            this.tBody = new Sunny.UI.UITextBox();
            this.uiFlowLayoutPanelAttach = new Sunny.UI.UIFlowLayoutPanel();
            this.uiGroupBox1.SuspendLayout();
            this.uiGroupBox2.SuspendLayout();
            this.uiGroupBox3.SuspendLayout();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.tPassword);
            this.uiGroupBox1.Controls.Add(this.uiLabel5);
            this.uiGroupBox1.Controls.Add(this.tUserName);
            this.uiGroupBox1.Controls.Add(this.uiLabel3);
            this.uiGroupBox1.Controls.Add(this.tPort);
            this.uiGroupBox1.Controls.Add(this.uiLabel4);
            this.uiGroupBox1.Controls.Add(this.tServiceAddress);
            this.uiGroupBox1.Controls.Add(this.uiComboBoxProtocol);
            this.uiGroupBox1.Controls.Add(this.uiLabel2);
            this.uiGroupBox1.Controls.Add(this.uiLabel1);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBox1.Location = new System.Drawing.Point(5, 40);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox1.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox1.Size = new System.Drawing.Size(997, 72);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.Text = "发件人";
            this.uiGroupBox1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel1.Location = new System.Drawing.Point(180, 34);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(103, 23);
            this.uiLabel1.TabIndex = 2;
            this.uiLabel1.Text = "服务地址：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel2.Location = new System.Drawing.Point(18, 32);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(66, 23);
            this.uiLabel2.TabIndex = 3;
            this.uiLabel2.Text = "协议：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiComboBoxProtocol
            // 
            this.uiComboBoxProtocol.DataSource = null;
            this.uiComboBoxProtocol.FillColor = System.Drawing.Color.White;
            this.uiComboBoxProtocol.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiComboBoxProtocol.Items.AddRange(new object[] {
            "SMTP",
            "Exchange"});
            this.uiComboBoxProtocol.Location = new System.Drawing.Point(72, 32);
            this.uiComboBoxProtocol.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBoxProtocol.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBoxProtocol.Name = "uiComboBoxProtocol";
            this.uiComboBoxProtocol.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBoxProtocol.Size = new System.Drawing.Size(104, 29);
            this.uiComboBoxProtocol.TabIndex = 4;
            this.uiComboBoxProtocol.Text = "SMTP";
            this.uiComboBoxProtocol.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiComboBoxProtocol.Watermark = "请选择协议";
            this.uiComboBoxProtocol.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tServiceAddress
            // 
            this.tServiceAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tServiceAddress.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tServiceAddress.Location = new System.Drawing.Point(267, 32);
            this.tServiceAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tServiceAddress.MinimumSize = new System.Drawing.Size(1, 16);
            this.tServiceAddress.Name = "tServiceAddress";
            this.tServiceAddress.ShowText = false;
            this.tServiceAddress.Size = new System.Drawing.Size(215, 29);
            this.tServiceAddress.TabIndex = 6;
            this.tServiceAddress.Text = "请输入服务地址";
            this.tServiceAddress.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tServiceAddress.Watermark = "";
            this.tServiceAddress.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tPort
            // 
            this.tPort.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPort.DoubleValue = 465D;
            this.tPort.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tPort.IntValue = 465;
            this.tPort.Location = new System.Drawing.Point(544, 32);
            this.tPort.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPort.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPort.Name = "tPort";
            this.tPort.ShowText = false;
            this.tPort.Size = new System.Drawing.Size(66, 29);
            this.tPort.TabIndex = 8;
            this.tPort.Text = "465";
            this.tPort.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPort.Type = Sunny.UI.UITextBox.UIEditType.Integer;
            this.tPort.Watermark = "端口号";
            this.tPort.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel4
            // 
            this.uiLabel4.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel4.Location = new System.Drawing.Point(491, 32);
            this.uiLabel4.Name = "uiLabel4";
            this.uiLabel4.Size = new System.Drawing.Size(103, 23);
            this.uiLabel4.TabIndex = 7;
            this.uiLabel4.Text = "端口：";
            this.uiLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel4.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tUserName
            // 
            this.tUserName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tUserName.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tUserName.Location = new System.Drawing.Point(675, 31);
            this.tUserName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tUserName.MinimumSize = new System.Drawing.Size(1, 16);
            this.tUserName.Name = "tUserName";
            this.tUserName.ShowText = false;
            this.tUserName.Size = new System.Drawing.Size(130, 29);
            this.tUserName.TabIndex = 10;
            this.tUserName.Text = "请输入账号";
            this.tUserName.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tUserName.Watermark = "";
            this.tUserName.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel3
            // 
            this.uiLabel3.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel3.Location = new System.Drawing.Point(621, 33);
            this.uiLabel3.Name = "uiLabel3";
            this.uiLabel3.Size = new System.Drawing.Size(71, 23);
            this.uiLabel3.TabIndex = 9;
            this.uiLabel3.Text = "账号：";
            this.uiLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel3.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tPassword
            // 
            this.tPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tPassword.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tPassword.Location = new System.Drawing.Point(866, 31);
            this.tPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tPassword.MinimumSize = new System.Drawing.Size(1, 16);
            this.tPassword.Name = "tPassword";
            this.tPassword.PasswordChar = '*';
            this.tPassword.ShowText = false;
            this.tPassword.Size = new System.Drawing.Size(110, 29);
            this.tPassword.TabIndex = 12;
            this.tPassword.Text = "请输入密码";
            this.tPassword.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tPassword.Watermark = "";
            this.tPassword.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel5
            // 
            this.uiLabel5.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel5.Location = new System.Drawing.Point(816, 32);
            this.uiLabel5.Name = "uiLabel5";
            this.uiLabel5.Size = new System.Drawing.Size(74, 23);
            this.uiLabel5.TabIndex = 11;
            this.uiLabel5.Text = "密码：";
            this.uiLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel5.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.tCCAddress);
            this.uiGroupBox2.Controls.Add(this.uiLabel7);
            this.uiGroupBox2.Controls.Add(this.tReceivers);
            this.uiGroupBox2.Controls.Add(this.uiLabel6);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBox2.Location = new System.Drawing.Point(5, 112);
            this.uiGroupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox2.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox2.Size = new System.Drawing.Size(997, 100);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.Text = "接收人";
            this.uiGroupBox2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tReceivers
            // 
            this.tReceivers.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tReceivers.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tReceivers.Location = new System.Drawing.Point(84, 29);
            this.tReceivers.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tReceivers.MinimumSize = new System.Drawing.Size(1, 16);
            this.tReceivers.Name = "tReceivers";
            this.tReceivers.ShowText = false;
            this.tReceivers.Size = new System.Drawing.Size(777, 29);
            this.tReceivers.TabIndex = 8;
            this.tReceivers.Text = "请输入收件人邮箱地址，以\";\"分隔，不可为空";
            this.tReceivers.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tReceivers.Watermark = "";
            this.tReceivers.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel6
            // 
            this.uiLabel6.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel6.Location = new System.Drawing.Point(18, 32);
            this.uiLabel6.Name = "uiLabel6";
            this.uiLabel6.Size = new System.Drawing.Size(86, 23);
            this.uiLabel6.TabIndex = 7;
            this.uiLabel6.Text = "收件人：";
            this.uiLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel6.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tCCAddress
            // 
            this.tCCAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tCCAddress.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tCCAddress.Location = new System.Drawing.Point(85, 61);
            this.tCCAddress.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tCCAddress.MinimumSize = new System.Drawing.Size(1, 16);
            this.tCCAddress.Name = "tCCAddress";
            this.tCCAddress.ShowText = false;
            this.tCCAddress.Size = new System.Drawing.Size(777, 29);
            this.tCCAddress.TabIndex = 10;
            this.tCCAddress.Text = "请输入抄送人邮箱地址，以\";\"分隔";
            this.tCCAddress.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tCCAddress.Watermark = "";
            this.tCCAddress.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel7
            // 
            this.uiLabel7.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel7.Location = new System.Drawing.Point(19, 64);
            this.uiLabel7.Name = "uiLabel7";
            this.uiLabel7.Size = new System.Drawing.Size(86, 23);
            this.uiLabel7.TabIndex = 9;
            this.uiLabel7.Text = "抄送人：";
            this.uiLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel7.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnSendEmail
            // 
            this.btnSendEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendEmail.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSendEmail.Location = new System.Drawing.Point(866, 29);
            this.btnSendEmail.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSendEmail.Name = "btnSendEmail";
            this.btnSendEmail.Size = new System.Drawing.Size(124, 67);
            this.btnSendEmail.Symbol = 61912;
            this.btnSendEmail.TabIndex = 13;
            this.btnSendEmail.Text = "发送";
            this.btnSendEmail.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSendEmail.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.uiFlowLayoutPanelAttach);
            this.uiGroupBox3.Controls.Add(this.btnSendEmail);
            this.uiGroupBox3.Controls.Add(this.btnAddAttach);
            this.uiGroupBox3.Controls.Add(this.uiLabel9);
            this.uiGroupBox3.Controls.Add(this.cbIsBodyHtml);
            this.uiGroupBox3.Controls.Add(this.tSubject);
            this.uiGroupBox3.Controls.Add(this.uiLabel8);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBox3.Location = new System.Drawing.Point(5, 212);
            this.uiGroupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox3.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.uiGroupBox3.Size = new System.Drawing.Size(997, 109);
            this.uiGroupBox3.TabIndex = 12;
            this.uiGroupBox3.Text = "邮件";
            this.uiGroupBox3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox3.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.tBody);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBox4.Location = new System.Drawing.Point(5, 321);
            this.uiGroupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBox4.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBox4.Size = new System.Drawing.Size(997, 328);
            this.uiGroupBox4.TabIndex = 13;
            this.uiGroupBox4.Text = "主体";
            this.uiGroupBox4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBox4.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tSubject
            // 
            this.tSubject.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tSubject.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.tSubject.Location = new System.Drawing.Point(85, 25);
            this.tSubject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tSubject.MinimumSize = new System.Drawing.Size(1, 16);
            this.tSubject.Name = "tSubject";
            this.tSubject.ShowText = false;
            this.tSubject.Size = new System.Drawing.Size(539, 29);
            this.tSubject.TabIndex = 10;
            this.tSubject.Text = "请输入邮件主题";
            this.tSubject.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tSubject.Watermark = "";
            this.tSubject.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel8
            // 
            this.uiLabel8.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel8.Location = new System.Drawing.Point(19, 28);
            this.uiLabel8.Name = "uiLabel8";
            this.uiLabel8.Size = new System.Drawing.Size(86, 23);
            this.uiLabel8.TabIndex = 9;
            this.uiLabel8.Text = "主题：";
            this.uiLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel8.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // cbIsBodyHtml
            // 
            this.cbIsBodyHtml.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbIsBodyHtml.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.cbIsBodyHtml.Location = new System.Drawing.Point(631, 25);
            this.cbIsBodyHtml.MinimumSize = new System.Drawing.Size(1, 1);
            this.cbIsBodyHtml.Name = "cbIsBodyHtml";
            this.cbIsBodyHtml.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbIsBodyHtml.Size = new System.Drawing.Size(100, 29);
            this.cbIsBodyHtml.TabIndex = 11;
            this.cbIsBodyHtml.Text = "是否网页";
            this.cbIsBodyHtml.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel9
            // 
            this.uiLabel9.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel9.Location = new System.Drawing.Point(19, 56);
            this.uiLabel9.Name = "uiLabel9";
            this.uiLabel9.Size = new System.Drawing.Size(86, 23);
            this.uiLabel9.TabIndex = 12;
            this.uiLabel9.Text = "附件：";
            this.uiLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel9.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnAddAttach
            // 
            this.btnAddAttach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddAttach.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.btnAddAttach.Location = new System.Drawing.Point(20, 81);
            this.btnAddAttach.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnAddAttach.Name = "btnAddAttach";
            this.btnAddAttach.Size = new System.Drawing.Size(43, 20);
            this.btnAddAttach.Symbol = 61543;
            this.btnAddAttach.SymbolSize = 14;
            this.btnAddAttach.TabIndex = 14;
            this.btnAddAttach.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnAddAttach.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // tBody
            // 
            this.tBody.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tBody.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tBody.Location = new System.Drawing.Point(5, 32);
            this.tBody.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tBody.MinimumSize = new System.Drawing.Size(1, 16);
            this.tBody.Multiline = true;
            this.tBody.Name = "tBody";
            this.tBody.ShowText = false;
            this.tBody.Size = new System.Drawing.Size(987, 291);
            this.tBody.TabIndex = 0;
            this.tBody.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.tBody.Watermark = "请输入邮件主体内容";
            this.tBody.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiFlowLayoutPanelAttach
            // 
            this.uiFlowLayoutPanelAttach.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiFlowLayoutPanelAttach.Location = new System.Drawing.Point(85, 62);
            this.uiFlowLayoutPanelAttach.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiFlowLayoutPanelAttach.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiFlowLayoutPanelAttach.Name = "uiFlowLayoutPanelAttach";
            this.uiFlowLayoutPanelAttach.Padding = new System.Windows.Forms.Padding(2);
            this.uiFlowLayoutPanelAttach.RectSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.None;
            this.uiFlowLayoutPanelAttach.ShowText = false;
            this.uiFlowLayoutPanelAttach.Size = new System.Drawing.Size(761, 42);
            this.uiFlowLayoutPanelAttach.TabIndex = 15;
            this.uiFlowLayoutPanelAttach.Text = "uiFlowLayoutPanel1";
            this.uiFlowLayoutPanelAttach.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiFlowLayoutPanelAttach.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // EmailForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1007, 654);
            this.Controls.Add(this.uiGroupBox4);
            this.Controls.Add(this.uiGroupBox3);
            this.Controls.Add(this.uiGroupBox2);
            this.Controls.Add(this.uiGroupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "EmailForm";
            this.Padding = new System.Windows.Forms.Padding(5, 40, 5, 5);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "邮件发送";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Sunny.UI.UIGroupBox uiGroupBox1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIComboBox uiComboBoxProtocol;
        private Sunny.UI.UITextBox tServiceAddress;
        private Sunny.UI.UITextBox tPort;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox tUserName;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox tPassword;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIGroupBox uiGroupBox2;
        private Sunny.UI.UITextBox tReceivers;
        private Sunny.UI.UILabel uiLabel6;
        private Sunny.UI.UITextBox tCCAddress;
        private Sunny.UI.UILabel uiLabel7;
        private Sunny.UI.UISymbolButton btnSendEmail;
        private Sunny.UI.UIGroupBox uiGroupBox3;
        private Sunny.UI.UIGroupBox uiGroupBox4;
        private Sunny.UI.UITextBox tSubject;
        private Sunny.UI.UILabel uiLabel8;
        private Sunny.UI.UICheckBox cbIsBodyHtml;
        private Sunny.UI.UILabel uiLabel9;
        private Sunny.UI.UISymbolButton btnAddAttach;
        private Sunny.UI.UITextBox tBody;
        private Sunny.UI.UIFlowLayoutPanel uiFlowLayoutPanelAttach;
    }
}