﻿using Base.Platform;
using DevKit.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevKit
{
    public class DevModule : ModuleList
    {
        public override int GetSymbol()
        {
            return 57358;
        }

        public override string GetName()
        {
            return "开发工具";
        }

        public override Module[] GetModules()
        {
            return new Module[] {
                new Module { ModuleId = 0x00040001u, ModuleName = "开发工具", FormType = typeof(DefaultForm), ModuleImage = Resource.Dev },
                new Module { ModuleId = 0x00040002u, ModuleName = "邮件发送", FormType = typeof(EmailForm), ModuleImage = Resource.Email },
            };
        }

        public override bool Init()
        {
            return true;
        }
    }
}
