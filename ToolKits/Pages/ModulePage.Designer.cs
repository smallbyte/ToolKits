﻿namespace ToolKits.Pages
{
    partial class ModulePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiFlowLayoutPanelMain = new Sunny.UI.UIFlowLayoutPanel();
            this.SuspendLayout();
            // 
            // uiFlowLayoutPanelMain
            // 
            this.uiFlowLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiFlowLayoutPanelMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiFlowLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.uiFlowLayoutPanelMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiFlowLayoutPanelMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiFlowLayoutPanelMain.Name = "uiFlowLayoutPanelMain";
            this.uiFlowLayoutPanelMain.Padding = new System.Windows.Forms.Padding(25);
            this.uiFlowLayoutPanelMain.ShowText = false;
            this.uiFlowLayoutPanelMain.Size = new System.Drawing.Size(800, 450);
            this.uiFlowLayoutPanelMain.TabIndex = 0;
            this.uiFlowLayoutPanelMain.Text = "uiFlowLayoutPanel1";
            this.uiFlowLayoutPanelMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiFlowLayoutPanelMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // ModulePage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.uiFlowLayoutPanelMain);
            this.Name = "ModulePage";
            this.Text = "ModulePage";
            this.Load += new System.EventHandler(this.ModulePage_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIFlowLayoutPanel uiFlowLayoutPanelMain;
    }
}