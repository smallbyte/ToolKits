﻿using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolKits.Pages
{
    public partial class HomePage : UIPage
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void HomePage_Initialize(object sender, EventArgs e)
        {
            List<string> catalogs = Base.Platform.Module.Catalogs;
        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            List<string> catalogs = Base.Platform.Module.Catalogs;
            foreach (string catalog in catalogs)
            {
                List<ModuleList> moduleLists = Base.Platform.Module.GetModuleLists(catalog);
                this.renderCatalogModules(catalog, moduleLists);
            }
        }

        private UILine getUILine(string catalog)
        {
            UILine line = new UILine();
            line.Dock = System.Windows.Forms.DockStyle.Top;
            line.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            line.MinimumSize = new System.Drawing.Size(1, 1);
            line.Name = catalog;
            line.Height = 20;
            line.Text = catalog;
            line.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            line.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            return line;
        }

        private void renderCatalogModules(string catalog, List<ModuleList> moduleLists)
        {
            if (moduleLists != null && moduleLists.Count > 0)
            {
                foreach (ModuleList moduleList in moduleLists)
                {
                    if (moduleList.GetModules() != null)
                    {
                        Module[] modules = moduleList.GetModules();
                        foreach (Module module in modules)
                        {
                            module.Catalog = catalog;
                            module.Group = moduleList.GetName();
                            UIImageButton uiImageButton = new UIImageButton();
                            uiImageButton.Tag = module;
                            uiImageButton.Text = module.ModuleName;
                            uiImageButton.Image = module.ModuleImage;
                            uiImageButton.ImageOffset = new System.Drawing.Point(25, 15);
                            uiImageButton.Size = new System.Drawing.Size(100, 100);
                            uiImageButton.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
                            uiImageButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
                            uiImageButton.Click += UiImageButton_Click;
                            uiImageButton.MouseHover += UiImageButton_MouseHover;
                            this.uiFlowLayoutPanelMain.Add(uiImageButton);
                        }
                    }
                }
            }
        }

        private void UiImageButton_MouseHover(object sender, EventArgs e)
        {
            UIImageButton uiImageButton = (UIImageButton)sender;
            Module module = uiImageButton.Tag as Module;
            if (module != null)
            {
                ToolTip toolTip = new ToolTip();
                toolTip.AutoPopDelay = 5000;//提示信息的可见时间
                toolTip.InitialDelay = 500;//事件触发多久后出现提示
                toolTip.ReshowDelay = 500;//指针从一个控件移向另一个控件时，经过多久才会显示下一个提示框
                toolTip.ShowAlways = true;//是否显示提示框

                //  设置伴随的对象.
                toolTip.SetToolTip(uiImageButton, string.Format("{0} - {1} - {2}", module.Catalog, module.Group, module.ModuleName));//设置提示按钮和提示内容
            }
        }

        private void UiImageButton_Click(object sender, EventArgs e)
        {
            Module module = ((UIImageButton)sender).Tag as Module;
            if (module != null)
                module.Open();
            else
                ShowWarningTip("该模块不可用！");
        }
    }
}
