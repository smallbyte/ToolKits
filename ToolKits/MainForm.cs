﻿using Base.Common;
using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolKits.Pages;

namespace ToolKits
{
    public partial class MainForm : UIAsideMainFrame
    {

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //ShowWaitForm("准备开始...");
            //Thread.Sleep(1000);
            //SetWaitFormDescription(UILocalize.SystemProcessing + "20%");
            //Thread.Sleep(1000);
            //SetWaitFormDescription(UILocalize.SystemProcessing + "40%");
            //Thread.Sleep(1000);
            //SetWaitFormDescription(UILocalize.SystemProcessing + "60%");
            //Thread.Sleep(1000);
            //SetWaitFormDescription(UILocalize.SystemProcessing + "80%");
            //Thread.Sleep(1000);
            //SetWaitFormDescription(UILocalize.SystemProcessing + "100%");
            //HideWaitForm();

            Base.Platform.Module.OpenModuleHandler = this.OpenModule;

            this.CreateMemus();
        }

        private void CreateMemus()
        {
            List<string> catalogs = Base.Platform.Module.Catalogs;
            int pageIndex = 1000;

            TreeNode home = Aside.CreateNode(AddPage(new HomePage(), ++pageIndex));
            if (catalogs != null)
            {
                foreach (string catalog in catalogs)
                {
                    List<ModuleList> moduleLists = Base.Platform.Module.GetModuleLists(catalog);
                    if (moduleLists != null && moduleLists.Count > 0)
                    {
                        TreeNode node = Aside.CreateNode(catalog, 57484, 24, ++pageIndex);
                        foreach (ModuleList moduleList in moduleLists)
                        {
                            if (moduleList.GetModules() != null)
                            {
                                TreeNode subNode = Aside.CreateChildNode(node, AddPage(new ModulePage(moduleList), ++pageIndex));
                                Aside.SetNodeSymbol(subNode, moduleList.GetSymbol());
                            }
                        }
                    }
                }
            }
            Aside.SelectFirst();
        }

        private Form OpenModule(Base.Platform.Module module, bool minimize)
        {
            try
            {
                foreach (Form child in this.MdiChildren)
                {
                    Base.Platform.Module _module = (Base.Platform.Module)child.Tag;
                    if (_module != null && _module.ModuleName == module.Catalog)
                    {
                        child.Activate();
                        return child;
                    }
                }
                Form form = (Form)module.FormType.GetConstructor(new Type[1] { typeof(Base.Platform.Module) }).Invoke(new object[1] { module });
                form.TopLevel = true;
                form.Show();
                //form.TopLevel = true;
                //form.ShowDialog();
                return form;
            }
            catch (Exception ex)
            {
                ExceptionManager.TraceWrite(ex, module.ModuleName);
                return null;
            }
        }
    }
}
