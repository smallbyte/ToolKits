﻿namespace ToolKits
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiProcessBarMain = new Sunny.UI.UIProcessBar();
            this.uiLabelMsg = new Sunny.UI.UILabel();
            this.SuspendLayout();
            // 
            // uiProcessBarMain
            // 
            this.uiProcessBarMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiProcessBarMain.Location = new System.Drawing.Point(33, 71);
            this.uiProcessBarMain.MinimumSize = new System.Drawing.Size(70, 3);
            this.uiProcessBarMain.Name = "uiProcessBarMain";
            this.uiProcessBarMain.Size = new System.Drawing.Size(377, 30);
            this.uiProcessBarMain.TabIndex = 0;
            this.uiProcessBarMain.Text = "加载中...";
            this.uiProcessBarMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabelMsg
            // 
            this.uiLabelMsg.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabelMsg.Location = new System.Drawing.Point(33, 45);
            this.uiLabelMsg.Name = "uiLabelMsg";
            this.uiLabelMsg.Size = new System.Drawing.Size(377, 23);
            this.uiLabelMsg.TabIndex = 1;
            this.uiLabelMsg.Text = "加载中...";
            this.uiLabelMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabelMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // StartForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(443, 130);
            this.Controls.Add(this.uiLabelMsg);
            this.Controls.Add(this.uiProcessBarMain);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StartForm";
            this.ShowIcon = false;
            this.Text = "正在启动，请稍候...";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.Load += new System.EventHandler(this.StartForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIProcessBar uiProcessBarMain;
        private Sunny.UI.UILabel uiLabelMsg;
    }
}