﻿using Base.Common;
using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolKits
{
    public partial class StartForm : UIForm
    {
        private Thread preloadThread;
        private List<Assembly> assemblies;
        public StartForm()
        {
            InitializeComponent();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            this.LoadModules();
        }

        private int loadingPercent;
        private string progressText;
        public void SetProgress(int _loadingPercent, string _progressText)
        {
            this.loadingPercent = _loadingPercent;
            this.progressText = _progressText;
            Utils.Invoke(this, this.UpdateUI);
        }

        public void UpdateUI()
        {
            this.uiProcessBarMain.Value = this.loadingPercent;
            if (this.loadingPercent == 100)
            {
                //Thread.Sleep(1000);
                this.DialogResult = DialogResult.OK;
            }
            else
                uiLabelMsg.Text = this.progressText;
        }

        private void LoadModules()
        {
            uiLabelMsg.Text = "正在加载模块...";
            preloadThread = new Thread(() =>
            {
                Dictionary<uint, Base.Platform.Module> modules = new Dictionary<uint, Base.Platform.Module>();
                modules.Add(1, new Base.Platform.Module { ModuleId = 1, FormType = typeof(MainForm) });
                this.assemblies = Base.Platform.Module.Load(AppDomain.CurrentDomain.BaseDirectory, modules, this.SetProgress);
                this.SetProgress(100, "模块加载完成！");
            });
            preloadThread.Start();
        }
    }
}
