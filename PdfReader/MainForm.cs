﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PdfView
{
    public partial class MainForm : UIForm
    {
        private string pdfPath;
        public MainForm(string[] args)
        {
            InitializeComponent();

            if (args != null && args.Length == 1)
            {
                this.pdfPath = args[0];
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (this.pdfPath != null)
            {
                this.showPdfFiew(this.pdfPath);
            }
        }

        private void pdfViewerMain_DragEnter(object sender, DragEventArgs e)
        {
            string path = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            this.showPdfFiew(path);
        }

        private void showPdfFiew(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                ShowErrorDialog("异常", "请选择文件！");
                return;
            }
            if (System.IO.Path.GetExtension(path).ToUpper() != ".PDF")
            {
                ShowErrorDialog("异常", "无法预览非Pdf文件！");
                //MessageBox.Show("无法预览非Pdf文件！", "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (path.Length > 50)
                    this.Text = string.Format("{0}...{1}", path.Left(3), path.Right(40));
                else
                    this.Text = path;
                pdfViewerMain.Document = PdfiumViewer.PdfDocument.Load(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
