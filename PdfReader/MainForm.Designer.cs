﻿namespace PdfView
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pdfViewerMain = new PdfiumViewer.PdfViewer();
            this.SuspendLayout();
            // 
            // pdfViewerMain
            // 
            this.pdfViewerMain.AllowDrop = true;
            this.pdfViewerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pdfViewerMain.Location = new System.Drawing.Point(0, 35);
            this.pdfViewerMain.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pdfViewerMain.Name = "pdfViewerMain";
            this.pdfViewerMain.Size = new System.Drawing.Size(1405, 737);
            this.pdfViewerMain.TabIndex = 0;
            this.pdfViewerMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.pdfViewerMain_DragEnter);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1405, 772);
            this.Controls.Add(this.pdfViewerMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "查看Pdf内容";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private PdfiumViewer.PdfViewer pdfViewerMain;
    }
}

