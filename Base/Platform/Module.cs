﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Base.Platform
{
    public class Module
    {
        public string Catalog { get; set; }
        public uint ModuleId { get; set; }
        public string ModuleName { get; set; }

        private Image moduleImage;
        public Image ModuleImage
        {
            get
            {
                if (moduleImage != null)
                    return moduleImage;
                return Resource.module;
            }
            set
            {
                moduleImage = value;
            }
        }
        public string Group { get; set; }
        public Type FormType { get; set; }

        private static List<ModuleList> moduleLists;
        public static List<ModuleList> ModuleLists
        {
            get
            {
                return moduleLists;
            }
            set
            {
                moduleLists = value;
            }
        }

        public static List<ModuleList> GetModuleLists(string catalog)
        {
            return moduleLists.FindAll(delegate (ModuleList list)
            {
                return list.Catalog == catalog;
            });
        }

        public static List<string> Catalogs
        {
            get
            {
                return catalogs;
            }
            set
            {
                catalogs = value;
            }
        }

        public static Dictionary<uint, Module> All
        {
            get
            {
                return modules;
            }
            set
            {
                modules = value;
            }
        }

        public static Module Get(uint moduleId)
        {
            Module module;
            if (modules.TryGetValue(moduleId, out module))
                return module;
            return null;
        }

        public delegate Form OpenModule(Module module, bool minimize);
        private static OpenModule openModuleHandler;
        public static OpenModule OpenModuleHandler
        {
            set
            {
                openModuleHandler = value;
            }
        }

        public Form Open()
        {
            if (openModuleHandler != null)
                return openModuleHandler(this, false);
            return null;
        }

        public static Form Open(uint moduleId)
        {
            Module module;
            if (modules.TryGetValue(moduleId, out module))
                return module.Open();
            return null;
        }

        private static List<string> catalogs;
        private static Dictionary<uint, Module> modules;
        public delegate void SetProgress(int _loadingPercent, string _progressText);
        public static List<Assembly> Load(string filePath, Dictionary<uint, Module> all, SetProgress setProgress)
        {
            modules = new Dictionary<uint, Module>();
            List<Assembly> assemblies = null;
            try
            {

                string dllPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "plugins");
                DirectoryInfo root = new DirectoryInfo(dllPath);
                FileInfo[] files = root.GetFiles("*Kit.dll");
                int dllCount = files.Length;
                int loaded = 0;
                int step = 100 / dllCount;

                assemblies = new List<Assembly>();
                List<ModuleList> moduleLists = new List<ModuleList>();
                foreach (FileInfo f in files)
                {
                    try
                    {

                        string dllName = f.Name;
                        setProgress(loaded / dllCount, string.Format("正在加载{0}...", dllName));
#if DEBUG
                        Assembly asm = Assembly.LoadFrom(f.FullName);
#else
                        byte[] bytes = File.ReadAllBytes(f.FullName);
                        Assembly asm = Assembly.Load(bytes);
#endif
                        string catalog = ((AssemblyTitleAttribute)Attribute.GetCustomAttribute(asm, typeof(AssemblyTitleAttribute))).Title;
                        if (catalogs == null)
                            catalogs = new List<string>();
                        if (!catalogs.Contains(catalog))
                            catalogs.Add(catalog);
                        foreach (Type type in asm.GetExportedTypes())
                        {
                            if (type.BaseType == typeof(ModuleList))
                            {
                                ModuleList modulelist = (ModuleList)type.GetConstructor(new Type[0]).Invoke(new object[0]);
                                if (modulelist.Init())
                                {
                                    modulelist.Catalog = catalog;
                                    moduleLists.Add(modulelist);
                                    foreach (Module module in modulelist.GetModules())
                                    {
                                        module.Catalog = catalog;
                                        module.Group = modulelist.GetName();
                                        if (!all.ContainsKey(module.ModuleId))
                                            all.Add(module.ModuleId, module);
                                    }
                                }
                                assemblies.Add(asm);
                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        ExceptionManager.TraceWrite(ex1);
                    }
                    loaded = loaded + step;
                }
                Module.modules = all;
                Module.moduleLists = moduleLists;
            }
            catch (Exception ex2)
            {
                ExceptionManager.TraceWrite(ex2);
            }
            if (all.Count == 0)
                return null;

            return assemblies;
        }
    }
}
