﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Base.Platform
{
    public abstract class ModuleList
    {
        public string Catalog { get; set; }
        public abstract string GetName();
        public virtual int GetSymbol()
        {
            return 57484;
        }

        public abstract Module[] GetModules();
        public virtual bool Init() { return true; }
    }
}
