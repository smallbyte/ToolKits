﻿using Base.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SystemKit.Pages;

namespace SystemKit
{
    public class DevModule : ModuleList
    {
        public override int GetSymbol()
        {
            return 61459;
        }

        public override string GetName()
        {
            return "系统工具";
        }

        public override Module[] GetModules()
        {
            return new Module[] {
                new Module { ModuleId = 0x00030001u, ModuleName = "系统工具", FormType = typeof(DefaultForm), ModuleImage= Resource.Settings },
                new Module { ModuleId = 0x00030002u, ModuleName = "文件扫描", FormType = typeof(FileScanningForm), ModuleImage= Resource.SearchFile },
                new Module { ModuleId = 0x00030003u, ModuleName = "扫码", FormType = typeof(CodeScanForm), ModuleImage= Resource.Scan }
            };
        }

        public override bool Init()
        {
            return true;
        }
    }
}
