﻿using AForge.Video.DirectShow;
using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;

namespace SystemKit.Pages
{
    public partial class CodeScanForm : UIForm
    {
        public CodeScanForm(Module module)
        {
            InitializeComponent();
        }

        private void CodeScanForm_Load(object sender, EventArgs e)
        {
            this.loadDevices();
        }

        private FilterInfoCollection videoDevices;
        private void loadDevices()
        {
            uiComboBoxMain.Items.Clear();
            this.videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            if (videoDevices.Count > 0)
            {
                for (int i = 0; i < videoDevices.Count; i++)
                    uiComboBoxMain.Items.Add(videoDevices[i].Name);
                uiComboBoxMain.SelectedIndex = 0;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (uiComboBoxMain.SelectedIndex == -1)
                return;
            this.btnStart.Enabled = false;
            this.btnStop.Enabled = true;
            this.uiComboBoxMain.Enabled = false;
            freeCamera();
            VideoCaptureDevice videoSource = new VideoCaptureDevice(this.videoDevices[uiComboBoxMain.SelectedIndex].MonikerString);
            videoSource.VideoResolution = videoSource.VideoCapabilities[0];
            videoSourcePlayerMain.VideoSource = videoSource;
            videoSourcePlayerMain.NewFrame += VideoSourcePlayerMain_NewFrame;
            videoSourcePlayerMain.Start();
        }

        private void VideoSourcePlayerMain_NewFrame(object sender, ref Bitmap image)
        {
            Thread thread = new Thread((obj) =>
            {
                try
                {
                    Bitmap bitmap = obj as Bitmap;
                    BarcodeReader reader = new BarcodeReader();
                    reader.Options.CharacterSet = "UTF-8";
                    Result resultQR = reader.Decode(bitmap);

                    if (resultQR != null)
                    {
                        Base.Common.Utils.Invoke(this.uiListBoxMain, () =>
                        {
                            this.uiListBoxMain.Items.Add(string.Format("{0}:{1}", resultQR.BarcodeFormat, resultQR.Text));
                        });
                    }
                }
                catch (Exception ex)
                {
                }
            });
            thread.Start(new Bitmap(image));

            DateTime now = DateTime.Now;
            Graphics g = Graphics.FromImage(image);

            //绘制当前日期
            SolidBrush brush = new SolidBrush(Color.Red);
            g.DrawString(now.ToString(), this.Font, brush, new PointF(image.Width - 160, image.Height - 35));
            brush.Dispose();

            g.Dispose();
        }

        private void freeCamera()
        {
            if (videoSourcePlayerMain.VideoSource != null)
            {
                if (videoSourcePlayerMain.IsRunning)
                {
                    //videoSourcePlayerMain.Stop();
                    videoSourcePlayerMain.SignalToStop();
                    videoSourcePlayerMain.WaitForStop();
                }
                videoSourcePlayerMain.VideoSource = null;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            this.btnStart.Enabled = true;
            this.btnStop.Enabled = false;
            this.uiComboBoxMain.Enabled = true;
            this.freeCamera();
        }
    }
}
