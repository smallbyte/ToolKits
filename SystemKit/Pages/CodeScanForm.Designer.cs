﻿namespace SystemKit.Pages
{
    partial class CodeScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeScanForm));
            this.uiPanelTop = new Sunny.UI.UIPanel();
            this.uiComboBoxMain = new Sunny.UI.UIComboBox();
            this.btnStart = new Sunny.UI.UISymbolButton();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.uiListBoxMain = new Sunny.UI.UIListBox();
            this.btnStop = new Sunny.UI.UISymbolButton();
            this.uiGroupBoxNow = new Sunny.UI.UIGroupBox();
            this.videoSourcePlayerMain = new AForge.Controls.VideoSourcePlayer();
            this.uiPanelTop.SuspendLayout();
            this.uiGroupBoxMain.SuspendLayout();
            this.uiGroupBoxNow.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiPanelTop
            // 
            this.uiPanelTop.Controls.Add(this.btnStop);
            this.uiPanelTop.Controls.Add(this.uiComboBoxMain);
            this.uiPanelTop.Controls.Add(this.btnStart);
            this.uiPanelTop.Controls.Add(this.uiLabel1);
            this.uiPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiPanelTop.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanelTop.Location = new System.Drawing.Point(5, 40);
            this.uiPanelTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanelTop.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanelTop.Name = "uiPanelTop";
            this.uiPanelTop.Size = new System.Drawing.Size(662, 42);
            this.uiPanelTop.TabIndex = 1;
            this.uiPanelTop.Text = null;
            this.uiPanelTop.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPanelTop.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiComboBoxMain
            // 
            this.uiComboBoxMain.DataSource = null;
            this.uiComboBoxMain.FillColor = System.Drawing.Color.White;
            this.uiComboBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiComboBoxMain.Location = new System.Drawing.Point(120, 6);
            this.uiComboBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiComboBoxMain.MinimumSize = new System.Drawing.Size(63, 0);
            this.uiComboBoxMain.Name = "uiComboBoxMain";
            this.uiComboBoxMain.Padding = new System.Windows.Forms.Padding(0, 0, 30, 2);
            this.uiComboBoxMain.Size = new System.Drawing.Size(312, 29);
            this.uiComboBoxMain.TabIndex = 19;
            this.uiComboBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiComboBoxMain.Watermark = "请选择摄像头";
            this.uiComboBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnStart
            // 
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStart.Location = new System.Drawing.Point(448, 4);
            this.btnStart.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(89, 32);
            this.btnStart.Symbol = 61515;
            this.btnStart.TabIndex = 18;
            this.btnStart.Text = "启动";
            this.btnStart.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStart.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiLabel1.Location = new System.Drawing.Point(14, 8);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(113, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "摄像头列表：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.uiListBoxMain);
            this.uiGroupBoxMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBoxMain.Location = new System.Drawing.Point(5, 353);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(662, 152);
            this.uiGroupBoxMain.TabIndex = 7;
            this.uiGroupBoxMain.Text = "扫描结果";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiListBoxMain
            // 
            this.uiListBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBoxMain.FillColor = System.Drawing.Color.White;
            this.uiListBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiListBoxMain.Location = new System.Drawing.Point(5, 32);
            this.uiListBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiListBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiListBoxMain.Name = "uiListBoxMain";
            this.uiListBoxMain.Padding = new System.Windows.Forms.Padding(2);
            this.uiListBoxMain.ShowText = false;
            this.uiListBoxMain.Size = new System.Drawing.Size(652, 115);
            this.uiListBoxMain.TabIndex = 0;
            this.uiListBoxMain.Text = "uiListBox1";
            this.uiListBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnStop
            // 
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStop.Enabled = false;
            this.btnStop.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnStop.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnStop.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(115)))), ((int)(((byte)(115)))));
            this.btnStop.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnStop.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnStop.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStop.Location = new System.Drawing.Point(543, 4);
            this.btnStop.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnStop.Name = "btnStop";
            this.btnStop.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.btnStop.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(115)))), ((int)(((byte)(115)))));
            this.btnStop.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnStop.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnStop.Size = new System.Drawing.Size(89, 32);
            this.btnStop.Style = Sunny.UI.UIStyle.Red;
            this.btnStop.Symbol = 62093;
            this.btnStop.TabIndex = 20;
            this.btnStop.Text = "停止";
            this.btnStop.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnStop.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // uiGroupBoxNow
            // 
            this.uiGroupBoxNow.Controls.Add(this.videoSourcePlayerMain);
            this.uiGroupBoxNow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxNow.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiGroupBoxNow.Location = new System.Drawing.Point(5, 82);
            this.uiGroupBoxNow.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxNow.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxNow.Name = "uiGroupBoxNow";
            this.uiGroupBoxNow.Padding = new System.Windows.Forms.Padding(5, 32, 5, 5);
            this.uiGroupBoxNow.Size = new System.Drawing.Size(662, 271);
            this.uiGroupBoxNow.TabIndex = 8;
            this.uiGroupBoxNow.Text = "扫描视图";
            this.uiGroupBoxNow.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxNow.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // videoSourcePlayerMain
            // 
            this.videoSourcePlayerMain.BorderColor = System.Drawing.Color.Transparent;
            this.videoSourcePlayerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.videoSourcePlayerMain.Location = new System.Drawing.Point(5, 32);
            this.videoSourcePlayerMain.Name = "videoSourcePlayerMain";
            this.videoSourcePlayerMain.Size = new System.Drawing.Size(652, 234);
            this.videoSourcePlayerMain.TabIndex = 4;
            this.videoSourcePlayerMain.Text = "...";
            this.videoSourcePlayerMain.VideoSource = null;
            // 
            // CodeScanForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(672, 510);
            this.Controls.Add(this.uiGroupBoxNow);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Controls.Add(this.uiPanelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CodeScanForm";
            this.Padding = new System.Windows.Forms.Padding(5, 40, 5, 5);
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "扫码";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.Load += new System.EventHandler(this.CodeScanForm_Load);
            this.uiPanelTop.ResumeLayout(false);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.uiGroupBoxNow.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIPanel uiPanelTop;
        private Sunny.UI.UISymbolButton btnStart;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIComboBox uiComboBoxMain;
        private Sunny.UI.UIGroupBox uiGroupBoxMain;
        private Sunny.UI.UIListBox uiListBoxMain;
        private Sunny.UI.UISymbolButton btnStop;
        private Sunny.UI.UIGroupBox uiGroupBoxNow;
        private AForge.Controls.VideoSourcePlayer videoSourcePlayerMain;
    }
}