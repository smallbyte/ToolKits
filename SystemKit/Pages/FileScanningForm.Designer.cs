﻿namespace SystemKit.Pages
{
    partial class FileScanningForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileScanningForm));
            this.backgroundWorkerMain = new System.ComponentModel.BackgroundWorker();
            this.uiPanelTop = new Sunny.UI.UIPanel();
            this.uiCheckBoxAll = new Sunny.UI.UICheckBox();
            this.uiTextBoxFilters = new Sunny.UI.UITextBox();
            this.uiLabel2 = new Sunny.UI.UILabel();
            this.btnDounDO = new Sunny.UI.UISymbolButton();
            this.btnSelectPath = new Sunny.UI.UISymbolButton();
            this.uiSelectedPath = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.uiPanelBottom = new Sunny.UI.UIPanel();
            this.lbMsg = new Sunny.UI.UILabel();
            this.uiGroupBoxMain = new Sunny.UI.UIGroupBox();
            this.checkedListBoxMain = new System.Windows.Forms.CheckedListBox();
            this.uiToolTipMain = new Sunny.UI.UIToolTip(this.components);
            this.uiButtonCheckAll = new Sunny.UI.UIButton();
            this.uiButtonDelete = new Sunny.UI.UIButton();
            this.uiPanelTop.SuspendLayout();
            this.uiPanelBottom.SuspendLayout();
            this.uiGroupBoxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorkerMain
            // 
            this.backgroundWorkerMain.WorkerSupportsCancellation = true;
            // 
            // uiPanelTop
            // 
            this.uiPanelTop.Controls.Add(this.uiCheckBoxAll);
            this.uiPanelTop.Controls.Add(this.uiTextBoxFilters);
            this.uiPanelTop.Controls.Add(this.uiLabel2);
            this.uiPanelTop.Controls.Add(this.btnDounDO);
            this.uiPanelTop.Controls.Add(this.btnSelectPath);
            this.uiPanelTop.Controls.Add(this.uiSelectedPath);
            this.uiPanelTop.Controls.Add(this.uiLabel1);
            this.uiPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiPanelTop.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanelTop.Location = new System.Drawing.Point(0, 35);
            this.uiPanelTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanelTop.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanelTop.Name = "uiPanelTop";
            this.uiPanelTop.Size = new System.Drawing.Size(726, 83);
            this.uiPanelTop.TabIndex = 0;
            this.uiPanelTop.Text = null;
            this.uiPanelTop.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPanelTop.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiCheckBoxAll
            // 
            this.uiCheckBoxAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiCheckBoxAll.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.uiCheckBoxAll.Location = new System.Drawing.Point(518, 45);
            this.uiCheckBoxAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiCheckBoxAll.Name = "uiCheckBoxAll";
            this.uiCheckBoxAll.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.uiCheckBoxAll.Size = new System.Drawing.Size(150, 29);
            this.uiCheckBoxAll.TabIndex = 21;
            this.uiCheckBoxAll.Text = "包含子文件夹";
            this.uiCheckBoxAll.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiTextBoxFilters
            // 
            this.uiTextBoxFilters.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBoxFilters.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiTextBoxFilters.Location = new System.Drawing.Point(117, 44);
            this.uiTextBoxFilters.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBoxFilters.MinimumSize = new System.Drawing.Size(1, 16);
            this.uiTextBoxFilters.Name = "uiTextBoxFilters";
            this.uiTextBoxFilters.ShowText = false;
            this.uiTextBoxFilters.Size = new System.Drawing.Size(387, 29);
            this.uiTextBoxFilters.TabIndex = 20;
            this.uiTextBoxFilters.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiTextBoxFilters.Watermark = "默认所有，正则匹配";
            this.uiTextBoxFilters.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiLabel2
            // 
            this.uiLabel2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel2.Location = new System.Drawing.Point(19, 45);
            this.uiLabel2.Name = "uiLabel2";
            this.uiLabel2.Size = new System.Drawing.Size(114, 23);
            this.uiLabel2.TabIndex = 19;
            this.uiLabel2.Text = "匹配规则：";
            this.uiLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel2.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // btnDounDO
            // 
            this.btnDounDO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDounDO.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDounDO.Location = new System.Drawing.Point(617, 8);
            this.btnDounDO.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnDounDO.Name = "btnDounDO";
            this.btnDounDO.Size = new System.Drawing.Size(89, 32);
            this.btnDounDO.Symbol = 61515;
            this.btnDounDO.TabIndex = 18;
            this.btnDounDO.Text = "开始";
            this.btnDounDO.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnDounDO.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnDounDO.Click += new System.EventHandler(this.btnDounDO_Click);
            // 
            // btnSelectPath
            // 
            this.btnSelectPath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPath.Location = new System.Drawing.Point(518, 8);
            this.btnSelectPath.MinimumSize = new System.Drawing.Size(1, 1);
            this.btnSelectPath.Name = "btnSelectPath";
            this.btnSelectPath.Size = new System.Drawing.Size(89, 32);
            this.btnSelectPath.Symbol = 61564;
            this.btnSelectPath.TabIndex = 17;
            this.btnSelectPath.Text = "选择";
            this.btnSelectPath.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelectPath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.btnSelectPath.Click += new System.EventHandler(this.btnSelectPath_Click);
            // 
            // uiSelectedPath
            // 
            this.uiSelectedPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiSelectedPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiSelectedPath.Location = new System.Drawing.Point(117, 11);
            this.uiSelectedPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiSelectedPath.MinimumSize = new System.Drawing.Size(1, 16);
            this.uiSelectedPath.Name = "uiSelectedPath";
            this.uiSelectedPath.ReadOnly = true;
            this.uiSelectedPath.ShowText = false;
            this.uiSelectedPath.Size = new System.Drawing.Size(387, 29);
            this.uiSelectedPath.TabIndex = 1;
            this.uiSelectedPath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiSelectedPath.Watermark = "双击文本框选择扫描路径";
            this.uiSelectedPath.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiSelectedPath.DoubleClick += new System.EventHandler(this.uiSelectedPath_DoubleClick);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(19, 11);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(114, 23);
            this.uiLabel1.TabIndex = 0;
            this.uiLabel1.Text = "扫描路径：";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uiLabel1.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiPanelBottom
            // 
            this.uiPanelBottom.Controls.Add(this.uiButtonDelete);
            this.uiPanelBottom.Controls.Add(this.uiButtonCheckAll);
            this.uiPanelBottom.Controls.Add(this.lbMsg);
            this.uiPanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiPanelBottom.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiPanelBottom.Location = new System.Drawing.Point(0, 385);
            this.uiPanelBottom.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiPanelBottom.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiPanelBottom.Name = "uiPanelBottom";
            this.uiPanelBottom.Size = new System.Drawing.Size(726, 29);
            this.uiPanelBottom.TabIndex = 1;
            this.uiPanelBottom.Text = null;
            this.uiPanelBottom.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiPanelBottom.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // lbMsg
            // 
            this.lbMsg.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbMsg.Location = new System.Drawing.Point(14, 4);
            this.lbMsg.Name = "lbMsg";
            this.lbMsg.Size = new System.Drawing.Size(490, 20);
            this.lbMsg.TabIndex = 0;
            this.lbMsg.Text = "...";
            this.lbMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbMsg.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // uiGroupBoxMain
            // 
            this.uiGroupBoxMain.Controls.Add(this.checkedListBoxMain);
            this.uiGroupBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBoxMain.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiGroupBoxMain.Location = new System.Drawing.Point(0, 118);
            this.uiGroupBoxMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiGroupBoxMain.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiGroupBoxMain.Name = "uiGroupBoxMain";
            this.uiGroupBoxMain.Padding = new System.Windows.Forms.Padding(5, 37, 5, 5);
            this.uiGroupBoxMain.Size = new System.Drawing.Size(726, 267);
            this.uiGroupBoxMain.TabIndex = 3;
            this.uiGroupBoxMain.Text = "扫描结果";
            this.uiGroupBoxMain.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.uiGroupBoxMain.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            // 
            // checkedListBoxMain
            // 
            this.checkedListBoxMain.CheckOnClick = true;
            this.checkedListBoxMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxMain.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.checkedListBoxMain.FormattingEnabled = true;
            this.checkedListBoxMain.Location = new System.Drawing.Point(5, 37);
            this.checkedListBoxMain.Name = "checkedListBoxMain";
            this.checkedListBoxMain.Size = new System.Drawing.Size(716, 225);
            this.checkedListBoxMain.TabIndex = 0;
            this.checkedListBoxMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this.checkedListBoxMain_MouseMove);
            // 
            // uiToolTipMain
            // 
            this.uiToolTipMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(54)))), ((int)(((byte)(54)))));
            this.uiToolTipMain.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.uiToolTipMain.OwnerDraw = true;
            // 
            // uiButtonCheckAll
            // 
            this.uiButtonCheckAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButtonCheckAll.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.uiButtonCheckAll.Location = new System.Drawing.Point(556, 5);
            this.uiButtonCheckAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButtonCheckAll.Name = "uiButtonCheckAll";
            this.uiButtonCheckAll.Size = new System.Drawing.Size(77, 20);
            this.uiButtonCheckAll.TabIndex = 1;
            this.uiButtonCheckAll.Text = "全/非全选";
            this.uiButtonCheckAll.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButtonCheckAll.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiButtonCheckAll.Click += new System.EventHandler(this.uiButtonCheckAll_Click);
            // 
            // uiButtonDelete
            // 
            this.uiButtonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uiButtonDelete.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.uiButtonDelete.Location = new System.Drawing.Point(640, 5);
            this.uiButtonDelete.MinimumSize = new System.Drawing.Size(1, 1);
            this.uiButtonDelete.Name = "uiButtonDelete";
            this.uiButtonDelete.Size = new System.Drawing.Size(71, 20);
            this.uiButtonDelete.TabIndex = 2;
            this.uiButtonDelete.Text = "删除选中";
            this.uiButtonDelete.TipsFont = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiButtonDelete.ZoomScaleRect = new System.Drawing.Rectangle(0, 0, 0, 0);
            this.uiButtonDelete.Click += new System.EventHandler(this.uiButtonDelete_Click);
            // 
            // FileScanningForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(726, 414);
            this.Controls.Add(this.uiGroupBoxMain);
            this.Controls.Add(this.uiPanelBottom);
            this.Controls.Add(this.uiPanelTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FileScanningForm";
            this.ShowRadius = false;
            this.ShowShadow = true;
            this.ShowTitleIcon = true;
            this.Text = "文件扫描";
            this.ZoomScaleRect = new System.Drawing.Rectangle(19, 19, 800, 450);
            this.uiPanelTop.ResumeLayout(false);
            this.uiPanelBottom.ResumeLayout(false);
            this.uiGroupBoxMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorkerMain;
        private Sunny.UI.UIPanel uiPanelTop;
        private Sunny.UI.UIPanel uiPanelBottom;
        private Sunny.UI.UIGroupBox uiGroupBoxMain;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox uiSelectedPath;
        private Sunny.UI.UISymbolButton btnSelectPath;
        private Sunny.UI.UISymbolButton btnDounDO;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox uiTextBoxFilters;
        private Sunny.UI.UILabel lbMsg;
        private Sunny.UI.UICheckBox uiCheckBoxAll;
        private System.Windows.Forms.CheckedListBox checkedListBoxMain;
        private Sunny.UI.UIToolTip uiToolTipMain;
        private Sunny.UI.UIButton uiButtonCheckAll;
        private Sunny.UI.UIButton uiButtonDelete;
    }
}