﻿using Base.Platform;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemKit.Pages
{
    public partial class FileScanningForm : UIForm
    {
        private bool running = false;
        private bool checkedAll = false;
        public FileScanningForm(Module module)
        {
            InitializeComponent();
        }

        private void btnSelectPath_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog() { RootFolder = Environment.SpecialFolder.MyComputer, Description = "请选择图片所在的文件夹" })
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.uiSelectedPath.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }

        private void uiSelectedPath_DoubleClick(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog() { RootFolder = Environment.SpecialFolder.MyComputer, Description = "请选择图片所在的文件夹" })
            {
                if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    this.uiSelectedPath.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }

        private void doRunning(bool running, string msg)
        {
            this.running = running;
            this.uiTextBoxFilters.Enabled = !running;
            this.uiSelectedPath.Enabled = !running;
            this.btnSelectPath.Enabled = !running;
            this.uiButtonCheckAll.Enabled = !running;
            this.uiButtonDelete.Enabled = !running;
            this.uiCheckBoxAll.Enabled = !running;
            this.btnDounDO.Enabled = !running;

            lbMsg.Text = msg;
        }

        private void scanFiles(string path, bool includeSub, string filters, Action<string> action, Action<string> reportProgress)
        {
            try
            {
                if (reportProgress != null && !string.IsNullOrEmpty(path))
                    reportProgress(string.Format("扫描路径:{0}", path));
                DirectoryInfo dir = new DirectoryInfo(path);
                if (includeSub)
                {
                    DirectoryInfo[] dirs = dir.GetDirectories();
                    if (dirs != null && dirs.Length > 0)
                    {
                        foreach (DirectoryInfo info in dirs)
                        {
                            this.scanFiles(info.FullName, includeSub, filters, action, reportProgress);
                        }
                    }
                }
                FileInfo[] files = dir.GetFiles("*.*", SearchOption.TopDirectoryOnly);
                foreach (FileInfo file in files)
                {
                    string fileName = file.Name;
                    if (string.IsNullOrEmpty(filters) && action != null)
                        action(file.FullName);
                    else if (Regex.IsMatch(fileName, filters) && action != null)
                        action(file.FullName);
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnDounDO_Click(object sender, EventArgs e)
        {
            if (this.running)
            {
                this.doRunning(false, "取消扫描！");
                if (backgroundWorkerMain.IsBusy)
                    backgroundWorkerMain.CancelAsync();
                return;
            }
            if (string.IsNullOrEmpty(uiSelectedPath.Text))
            {
                ShowWarningDialog("提示", "请选择文件夹路径！");
                return;
            }
            this.doRunning(true, "开始扫描...");
            checkedAll = false;
            checkedListBoxMain.Items.Clear();
            string selectedPath = uiSelectedPath.Text;
            bool bIncludeAll = uiCheckBoxAll.Checked;
            string filters = string.IsNullOrEmpty(uiTextBoxFilters.Text) ? "" : uiTextBoxFilters.Text;
            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                backgroundWorkerMain.ReportProgress(0, "开始扫描文件...");
                this.scanFiles(selectedPath, bIncludeAll, filters, (file) =>
                {
                    Base.Common.Utils.Invoke(checkedListBoxMain, () =>
                    {
                        checkedListBoxMain.Items.Add(file);
                    });
                    backgroundWorkerMain.ReportProgress(0, string.Format("文件:{0}", file));
                }, (o) =>
                {
                    backgroundWorkerMain.ReportProgress(0, o);
                });
            }, (s2, e2) =>
            {
                lbMsg.Text = e2.UserState.ToString();
            }, (s3, e3) =>
            {
                this.doRunning(false, "扫描完成.");
            });
        }

        private void checkedListBoxMain_MouseMove(object sender, MouseEventArgs e)
        {
            int index = checkedListBoxMain.IndexFromPoint(e.Location);
            if (index != -1 && index < checkedListBoxMain.Items.Count)
            {
                if (uiToolTipMain.GetToolTip(checkedListBoxMain) != checkedListBoxMain.Items[index].ToString())
                {
                    uiToolTipMain.SetToolTip(checkedListBoxMain, checkedListBoxMain.Items[index].ToString());
                }
            }
        }

        private void uiButtonCheckAll_Click(object sender, EventArgs e)
        {
            checkedAll = !checkedAll;
            for (int index = 0; index < this.checkedListBoxMain.Items.Count; index++)
            {
                this.checkedListBoxMain.SetItemChecked(index, checkedAll);
            }
        }

        private void uiButtonDelete_Click(object sender, EventArgs e)
        {
            if (backgroundWorkerMain.IsBusy)
            {
                ShowWarningDialog("提示", "正忙，请稍候...");
                return;
            }
            List<string> files = new List<string>();
            foreach (object file in this.checkedListBoxMain.CheckedItems)
            {
                files.Add(file.ToString());
            }
            if (files.Count == 0)
            {
                ShowWarningDialog("提示", "请选中至少一个文件...");
                return;
            }
            this.doRunning(true, "开始处理...");
            Base.Common.Utils.RunBackgroundWorker(backgroundWorkerMain, (s1, e1) =>
            {
                backgroundWorkerMain.ReportProgress(0, "开始删除文件...");
                int total = files.Count;
                int index = 0;
                foreach (string file in files)
                {
                    index++;
                    FileInfo _file = new FileInfo(file);
                    _file.Attributes = _file.Attributes & ~(FileAttributes.Archive | FileAttributes.ReadOnly | FileAttributes.Hidden);
                    backgroundWorkerMain.ReportProgress(0, string.Format("删除文件:({0}/{1}),{2}", index, total, _file.FullName));
                    File.Delete(_file.FullName);
                    Base.Common.Utils.Invoke(checkedListBoxMain, () =>
                    {
                        checkedListBoxMain.Items.Remove(file);
                    });
                }
            }, (s2, e2) =>
            {
                lbMsg.Text = e2.UserState.ToString();
            }, (s3, e3) =>
            {
                this.doRunning(false, "处理完成.");
            });
        }
    }
}
